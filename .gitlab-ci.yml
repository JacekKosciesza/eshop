image: circleci/node:latest-browsers

before_script:
  - cd src/web
  - npm ci

cache:
  paths:
    - node_modules/

stages:
  - test
  - lint
  - build
  - docker
  - deploy
  - performance
  - pages

# Test

test-web:
  stage: test
  script:
    - npm run test
  coverage: '/Statements.*?(\d+(?:\.\d+)?)%/'
  artifacts:
    paths:
      - src/web/coverage/

test-services:
  image: golang:latest
  stage: test
  before_script:
    - cd src/services/catalog
  script:
    - mkdir coverage
    - go test -coverprofile coverage/coverage.out
    - go tool cover -html coverage/coverage.out -o coverage/index.html
    - go tool cover -func coverage/coverage.out
  coverage: '/^coverage:\s(\d+(?:\.\d+)?%)/'
  artifacts:
    paths:
      - src/services/catalog/coverage/

# Lint

# lint-security:
#   stage: lint
#   script:
#     - npm audit

lint-prettier:
  stage: lint
  script:
    - npm run lint:prettier

lint-eslint:
  stage: lint
  script:
    - npm run lint:eslint

lint-types:
  stage: lint
  script:
    - npm run lint:types

# https://stackoverflow.com/questions/56251725/launching-sonar-scanner-from-a-gitlab-docker-runner
lint-sonarqube:
  image:
    name: newtmitch/sonar-scanner
    entrypoint: [""]
  stage: lint
  before_script:
    - cd src
  script:
    - cp web/coverage/lcov.info web/coverage/lcov.sonarqube
    - sed -i.bak 's#'$PWD/'##g' web/coverage/lcov.sonarqube
    - sed -i 's|eshop.wtf|services/catalog|g' services/catalog/coverage/coverage.out
    - sonar-scanner -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.login=$SONAR_LOGIN

# Build

build-web:
  stage: build
  script:
    - npm run build
  artifacts:
    paths:
      - src/web/dist/

build-storybook:
  stage: build
  script:
    - npm run site:build
  artifacts:
    paths:
      - src/web/_site/

build-catalog:
  image: golang:latest
  stage: build
  before_script:
    - cd src/services/catalog
  script:
    - go build -o main
  artifacts:
    paths:
      - src/services/catalog/main

# Docker

build-docker:
  image: docker:stable
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind
  stage: docker
  before_script:
    - docker info
  script:
    - cd src/web
    - docker build -t jkosciesza/eshop-web .
    - docker build -t jkosciesza/eshop-storybook -f ./.storybook/Dockerfile .
    - cd ../services/catalog
    - docker build -t jkosciesza/eshop-catalog .
    - docker images
    - echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin
    - docker push jkosciesza/eshop-web
    - docker push jkosciesza/eshop-storybook
    - docker push jkosciesza/eshop-catalog
  only:
    - master

# Performance

lighthouse:
  image: mrmanny/lighthouse
  stage: performance
  before_script:
    - cd src/web
  script:
    - lighthouse --version
    - mkdir lighthouse
    - lighthouse --chrome-flags="--headless --no-sandbox --disable-gpu --enable-logging" --output html --output json --output-path ./lighthouse/eshop.html $ESHOP_URL
    - cd lighthouse
    - mv ./eshop.report.html index.html
    - mv ./eshop.report.json report.json
  artifacts:
    paths:
      - src/web/lighthouse

# Deploy

kubernetes:
  image: google/cloud-sdk:latest
  stage: deploy
  before_script:
    - echo $GCLOUD_SERVICE_KEY > ${HOME}/gcloud-service-key.json
    - gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json
    - gcloud container clusters get-credentials eshop-cluster --zone europe-west3-a --project $GCLOUD_PROJECT_ID
    - cd src
  script:
    #- kubectl apply -f deploy.yml
    - kubectl rollout restart deploy web-deploy
    - kubectl rollout restart deploy storybook-deploy

# Pages

pages:
  image: alpine
  stage: pages
  dependencies:
    - test-web
    - test-services
    - lighthouse
  before_script:
    - echo "Deploying to GitLab pages"
    - echo $CI_PIPELINE_URL
    - echo $CI_COMMIT_TITLE
    - echo https://gitlab.com/JacekKosciesza/eshop/commit/$CI_COMMIT_SHA
  script:
    - cd src/web/.public
    - sed -i 's|CI_PIPELINE_URL|'$CI_PIPELINE_URL'|g' ./index.html
    - sed -i "s|CI_COMMIT_TITLE|${CI_COMMIT_TITLE}|g" ./index.html
    - sed -i 's|CI_COMMIT_SHA|'$CI_COMMIT_SHA'|g' ./index.html
    - cd ../../../
    - mkdir public
    - cp -r src/web/.public/* ./public
    - mkdir public/coverage/frontend/
    - mkdir public/coverage/backend/
    - mkdir public/lighthouse/
    - cp -r src/web/coverage/* ./public/coverage/frontend
    - cp -r src/services/catalog/coverage/* ./public/coverage/backend
    - cp -r src/web/lighthouse/* ./public/lighthouse
  artifacts:
    paths:
      - public
    expire_in: 30 days
