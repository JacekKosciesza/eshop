# eShop

[![pipeline status](https://gitlab.com/JacekKosciesza/eshop/badges/master/pipeline.svg)](https://gitlab.com/JacekKosciesza/eshop/commits/master)

[![coverage report](https://gitlab.com/JacekKosciesza/eshop/badges/master/coverage.svg)](https://gitlab.com/JacekKosciesza/eshop/commits/master)

[![Quality Gate Status](http://51.140.93.193/api/project_badges/measure?project=eshop&metric=alert_status)](http://51.140.93.193/dashboard?id=eshop)

## GitLab

## eShopOnContainers

- [CSS](https://github.com/dotnet-architecture/eShopOnContainers/blob/43695ca30a85c7edfe6f169109eafec7e91b8ca7/src/Web/WebMVC/wwwroot/css/catalog/catalog.component.css)

## Google Cloud

- [Publishing Google Cloud Container Registry Images from Gitlab CI](https://medium.com/@gaforres/publishing-google-cloud-container-registry-images-from-gitlab-ci-23c45356ff0e)

- https://about.gitlab.com/2016/11/03/publish-code-coverage-report-with-gitlab-pages/

## Docker

- Build docker images and push it to the Docker Hub

```
docker-compose up --build
docker images "jkosciesza\/*"
docker push jkosciesza/eshop-web
docker push jkosciesza/eshop-storybook
docker run -it --entrypoint bash newtmitch/sonar-scanner
```

- Check it: https://cloud.docker.com/u/jkosciesza/repository/list

### GitLab

- [Building Docker images with GitLab CI/CD](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
- [Predefined environment variables reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

## Kubernetes

- Create cluster in Google Cloud as described on PluralSight course - [Getting Started with Kubernetes](https://app.pluralsight.com/player?course=getting-started-kubernetes&author=nigel-poulton&name=getting-started-kubernetes-m4&clip=2&mode=live)
- Install [Google Cloud SDK](Installing Google Cloud SDK)
- Use option 'Connect to the cluster'

```
kubectl version --short
kubectl config view
gcloud container clusters get-credentials eshop-cluster --zone europe-west3-a --project noted-bliss-247910
```

- Check context `kubectl get nodes`, should print something like this

```
NAME                                     STATUS   ROLES    AGE    VERSION
gke-eshop-cluster-pool-1-3dc79404-l2cm   Ready    <none>   5d5h   v1.12.8-gke.10
gke-eshop-cluster-pool-1-3dc79404-n4vs   Ready    <none>   5d5h   v1.12.8-gke.10
gke-eshop-cluster-pool-1-3dc79404-s7x2   Ready    <none>   5d5h   v1.12.8-gke.10
```

- create pod

```
kubectl create -f pod.yml
kubectl get pods
kubectl describe pods | grep app
kubectl get pods/web-pod
kubectl get pods --all-namespaces
kubectl describe pods
kubectl delete pods web-pod
```

- create pod with replication controller (desired state model)

```
kubectl create -f rc.yml
kubectl get rc
kubectl get rc -o wide
kubectl describe rc
kubectl apply -f rc.yml
kubectl get pods
kubectl delete rc web-rc
```

- crate service

```
kubectl expose rc web-rc --name=web-svc --target-port=80 --type=NodePort
kubectl get svc
kubectl describe svc web-svc
kubectl get nodes -o wide
kubectl delete svc web-svc
kubectl delete svc web-svc -oyml
```

or

```
kubectl create -f svc.yml
```

- cluster

```
https://35.246.217.144/
https://35.246.217.144/ui
```

- nodes

```
35.198.106.227
35.246.249.220
35.198.87.192
```

- endpoints

```
kubectl get ep
kubectl describe ep web-svc
```

- deployments (crate + update)

```
kubectl create -f deploy.yml
kubectl describe deploy web-deploy
kubectl get rs
kubectl apply -f deploy.yml --record
kubectl rollout status deployment web-deploy
kubectl get deploy web-deploy
kubectl rollout history deployment web-deploy
kubectl get rs
kubectl rollout undo deployment web-deploy --to-revision=1
```

## GitLab

- pipline badge is in `Settings -> CI / CD -> General pipelines`

- code coverage: https://medium.com/@caleb.ukle/code-coverage-badge-with-angular-karma-istanbul-on-gitlab-ci-9611b69ad7e

## Go

`go mod init eshop`
`docker build -t jkosciesza/eshop-catalog .`
`docker run -p 80:80 jkosciesza/eshop-catalog`

## Performance

- [Performance Budget Calculator](https://perf-budget-calculator.firebaseapp.com/)

## Helm

```
choco install kubernetes-helm
```

https://github.com/phcollignon/helm/blob/master/lab2_kubectl_version2/yaml/backend.yaml

```
helm version --short
helm init
kubectl get all --namespace=kube-system -l name=tiller
helm create nginx-demo
```

https://github.com/docker/for-win/issues/1901#issuecomment-405016489
https://kubernetes.github.io/ingress-nginx/deploy/#docker-for-mac

helm install nginx-demo
kubectl get all | grep nginx-demo
helm reset

helm create test
helm install test --tiller-namespace=lab --namespace=default

helm list
helm delete billowing-peahen
kubectl get configmaps --namespace=kube-system
helm delete billowing-peahen --purge

helm init --history-max 200

SemVer 2.0

Chart: definition
Release: instance

Applicaiton version
Chart version
Release revision

Install a Release: helm install [chart]
Upgrade a Release version: helm upgrade [release][chart]
Rollback to a Release revision: helm rollback [release][revision]
Print Release history: helm history [release]
Display Release status: helm status [release]
Show derails of a Release: helm get [release]
Uninstall a Release: helm delete [release]
List Releases: helm list

helm list --short
helm status calling-whale | less

helm upgrade washed-kiwi eshop
helm list
helm rollback washed-kiwi 1
helm history washed-kiwi

helm get undercooked-anteater

# test templates

helm template mychart
helm template mychart -x templates/service.yml
helm install --dry-run --debug
helm install --dry-run --debug 2>&1 | less

helm install -f file
helm install --set foo=bar

helm template eshop | less

helm install eshop --name dev
helm install eshop --name dev --set web.config.eshop_env=DEV
helm install eshop --name test --set web.config.eshop_env=TEST

helm package eshop
helm repo index .
helm package --sign
helm verify chart.tgz
helm install --verify

helm package web storybook catalog
helm repo list

helm serve

helm repo add myrepo http://myserver.org/charts
helm repo remove myrepo

```
helm pacakge web
helm pacakge storybook
helm pacakge catalog
helm repo index .
cp *.tgz, *.html C:\Users\jacek\.helm\repository\local
cd C:\Users\jacek\.helm\repository\local
helm serve
```

```
helm dependency update eshop
helm dependency build eshop
helm dependency list eshop
```

```
helm install eshop --set catalog.enabled=true
helm install eshop --set tags.api=false
helm dependency list eshop
```

```
helm repo list
helm search keyword
helm inspect chart_name
helm inspect chart chart_name
helm inspect values chart_name
helm fetch chart_name
helm dependency update chart_name
```

https://hub.helm.sh/charts/stable/postgresql
https://github.com/helm/charts/tree/master/stable/postgresql
https://github.com/helm/charts/issues/9093

```
helm inspect stable/postgresql | less
helm dependency update eshop
kubectl logs dev-postgresql-0
kubectl exec dev-postgresql-0 cat /opt/bitnami/postgresql/logs/postgresql.log
```

cluster

```
kubectl logs dev-postgresql-master-0 dev-postgresql
kubectl describe pod/dev-postgresql-master-0 -n default
```

https://github.com/helm/helm/issues/3130

```
helm reset --force
kubectl --namespace kube-system create serviceaccount tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
#kubectl --namespace kube-system patch deploy tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
helm init
helm init --upgrade --service-account tiller
helm install eshop --name prod -f .\eshop\values-prod.yaml
```

## Terraform

```
choco install terraform -y
terraform init
terraform version
```

https://medium.com/@timhberry/learn-terraform-by-deploying-a-google-kubernetes-engine-cluster-a29071d9a6c2
https://cloud.google.com/compute/docs/machine-types

```
terraform plan
terraform plan -out myplan
terrafrom apply
terraform apply "myplan"
terraform destroy
```

```
terraform apply
gcloud container clusters get-credentials eshop-cluster --zone europe-west3-a --project noted-bliss-247910
gcloud container clusters get-credentials eshop-cluster --zone europe-west3-a kubectl apply -f .\tiller-rbac.yaml
helm init --service-account tiller
helm version --short
helm install eshop --name prod -f .\eshop\values-prod.yaml
helm status prod
helm upgrade -f .\eshop\values-prod.yaml prod eshop
helm delete prod --purge
```

test helm templates

```
helm template eshop --name dev --set web.config.eshop_env=DEV > dev.txt

```

TLS
https://www.youtube.com/watch?v=aN9nVa8yeBo
https://cert-manager.readthedocs.io/en/latest/reference/issuers.html
https://github.com/helm/charts/issues/10949
https://github.com/PacktPublishing/Hands-On-Kubernetes-on-Azure/issues/3
https://github.com/jetstack/cert-manager <--- user this
https://docs.cert-manager.io/en/latest/tutorials/acme/quick-start/
https://github.com/helm/helm/issues/2004
https://github.com/jetstack/cert-manager/issues/606
https://medium.com/omnius/kubernetes-ingress-gce-vs-nginx-controllers-1-3-d89d6dd3da73

```
helm dependency update eshop
kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.6/deploy/manifests/00-crds.yaml
helm upgrade --install cert-mgr stable/cert-manager
#helm install stable/cert-manager --name cert-manager --namespace kube-system --version v0.6.0 --set createCustomResource=true
kubectl apply -f .\cluster-issuer.yaml
kubectl get clusterissuers
```

helm install --name cert-manager --namespace cert-manager --version v0.9.1 jetstack/cert-manager

# Terraform

```
terraform apply
```

# TLS

```
terraform apply -auto-approve
gcloud container clusters get-credentials eshop-cluster --zone europe-west3-a --project noted-bliss-247910

kubectl create serviceaccount tiller --namespace=kube-system
kubectl create clusterrolebinding tiller-admin --serviceaccount=kube-system:tiller --clusterrole=cluster-admin
helm init --service-account=tiller
# helm version --short
helm repo update


helm install stable/nginx-ingress --name eshop
# kubectl get svc

kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.9/deploy/manifests/00-crds.yaml
kubectl create namespace cert-manager
kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install --name cert-manager --namespace cert-manager --version v0.9.1 jetstack/cert-manager

kubectl apply -f staging-issuer.yaml
kubectl apply -f production-issuer.yaml

helm install eshop --name prod -f .\eshop\values-prod.yaml

helm status prod
kubectl get ingress

...
kubectl delete secret quickstart-example-tls
helm upgrade -f .\eshop\values-prod.yaml prod eshop
helm template eshop --name prod -f .\eshop\values-prod.yaml > prod.txt
```

```
helm upgrade --recreate-pods -f .\eshop\values-prod.yaml prod eshop
```

## Service Worker

https://developers.google.com/web/tools/workbox/guides/using-bundlers#workbox-build
https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin#cache_additional_non-webpack_assets
https://github.com/Rainrider/rollup-plugin-workbox-build
https://developers.google.com/web/ilt/pwa/lab-workbox

# NGINX

`docker run -it -p 8080:80 jkosciesza/eshop-web`

# Images

```bash
sha256sum ./1.jpg | cut -c1-8
```

# Testing

https://medium.com/@anephenix/end-to-end-testing-single-page-apps-and-node-js-apis-with-cucumber-js-and-puppeteer-ad5a519ace0
https://github.com/GoogleChrome/puppeteer/issues/696
