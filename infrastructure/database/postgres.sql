CREATE TABLE "brands" (
    "id"          integer,
    PRIMARY KEY(id)
);

CREATE TABLE "brands_translations" (
    "id"          integer,
    "language"    varchar(3),
    "name"        varchar(50),
    PRIMARY KEY("id", "language"),
    FOREIGN KEY ("id") REFERENCES "brands" ("id")
);

INSERT INTO "brands" (
    "id"
) VALUES 
    (1),
    (2);

INSERT INTO "brands_translations" (
    "id",
    "language",
    "name"
) VALUES 
    -- en
    (1, 'en',  '.NET'),
    (2, 'en', 'Other'),
    -- pl
    (1, 'pl',  '.NET'),
    (2, 'pl', 'Inne');

CREATE TABLE "types" (
    "id"          integer,
    PRIMARY KEY(id)
);

CREATE TABLE "types_translations" (
    "id"          integer,
    "language"    varchar(3),
    "name"        varchar(50),
    PRIMARY KEY("id", "language"),
    FOREIGN KEY ("id") REFERENCES "types" ("id")
);

INSERT INTO "types" (
    "id"
) VALUES 
    (1),
    (2),
    (3),
    (4);


INSERT INTO "types_translations" (
    "id",
    "language",
    "name"
) VALUES
    -- en
    (1, 'en', 'Mug'),
    (2, 'en', 'T-Shirt'),
    (3, 'en', 'Sheet'),
    (4, 'en', 'Hoodie'),
    -- pl
    (1, 'pl', 'Kube'),
    (2, 'pl', 'Podkoszulek'),
    (3, 'pl', 'Płytka'),
    (4, 'pl', 'Bluza');


CREATE TABLE "items" (
    "id"          integer,
    "price"       decimal(10,2),
    "pictureUri"  varchar(100),
    "brandId"     integer,
    "typeId"      integer,
    PRIMARY KEY(id),
    FOREIGN KEY ("brandId") REFERENCES "brands" ("id"),
    FOREIGN KEY ("typeId") REFERENCES "types" ("id")
);

CREATE TABLE "items_translations" (
    "id"          integer,
    "language"    varchar(3),
    "name"        varchar(50),
    PRIMARY KEY("id", "language"),
    FOREIGN KEY ("id") REFERENCES "items" ("id")
);

CREATE INDEX "brandId_idx" ON "items" ("brandId");
CREATE INDEX "typeId_idx" ON "items" ("typeId");

INSERT INTO "items" (
    "id",
    "price",
    "pictureUri",
    "brandId",
    "typeId"
) VALUES 
    (1, 19.5, '/images/catalog/items/1.5cdf514d', 1, 4),
    (2, 8.5, '/images/catalog/items/2.9b8c354d', 1, 1),
    (3, 12, '/images/catalog/items/3.d0254d3d', 2, 2),
    (4, 12, '/images/catalog/items/4.206927c7', 1, 2),
    (5, 8.5, '/images/catalog/items/5.c80b6c1b', 2, 3),
    (6, 12, '/images/catalog/items/6.85793482', 1, 4),
    (7, 12, '/images/catalog/items/7.9bfd5f97', 2, 2),
    (8, 8.5, '/images/catalog/items/8.def8b245', 2, 4),
    (9, 12, '/images/catalog/items/9.8b39cb98', 2, 1),
    (10, 12, '/images/catalog/items/10.017e8910', 1, 3),
    (11, 8.5, '/images/catalog/items/11.a91168dc', 2, 3),
    (12, 12, '/images/catalog/items/12.2f7d231f', 2, 2);


INSERT INTO "items_translations" (
    "id",
    "language",
    "name"
) VALUES
    -- en
    (1,  'en', '.NET Bot Black Hoodie'),
    (2,  'en', '.NET Black & White Mug'),
    (3,  'en', 'Prism White T-Shirt'),
    (4,  'en', '.NET Foundation T-Shirt'),
    (5,  'en', 'Roslyn Red Sheet'),
    (6,  'en', '.NET Blue Hoodie'),
    (7,  'en', 'Roslyn Red T-Shirt'),
    (8,  'en', 'Kudu Purple Hoodie'),
    (9,  'en', 'Cup<T> White Mug'),
    (10, 'en', '.NET Foundation Sheet'),
    (11, 'en', 'CUP<T> Sheet'),
    (12, 'en', 'Prism White T-Shirt'),
    -- pl
    (1,  'pl', '.NET Bot Czarna Bluza'),
    (2,  'pl', '.NET Czarny & Biały White Kubek'),
    (3,  'pl', 'Prism Biały Podkoszulek'),
    (4,  'pl', '.NET Foundation Podkoszulek'),
    (5,  'pl', 'Roslyn Czerwona Płytka'),
    (6,  'pl', '.NET Niebieska Bluza'),
    (7,  'pl', 'Roslyn Czerwony Podkoszulek'),
    (8,  'pl', 'Kudu Fioletowy Bluza'),
    (9,  'pl', 'Cup<T> Biała Kubek'),
    (10, 'pl', '.NET Foundation Płytka'),
    (11, 'pl', 'CUP<T> Płytka'),
    (12, 'pl', 'Prism Biały Podkoszulek');