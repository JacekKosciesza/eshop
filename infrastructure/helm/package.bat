rm -r dist
mkdir dist
helm package eshop\charts\web --destination dist
helm package eshop\charts\storybook --destination dist
helm package eshop\charts\catalog --destination dist
helm repo index .\dist