provider "google" {
  credentials = "${file("account.json")}"
  project     = "noted-bliss-247910"
  region      = "europe-west3-a"
}
