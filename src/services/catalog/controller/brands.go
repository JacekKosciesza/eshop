package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"

	servertiming "github.com/mitchellh/go-server-timing"
	"gitlab.com/JacekKosciesza/eshop/model"
)

type brands struct{}

func (b brands) registerRoutes() {
	var h http.Handler = http.HandlerFunc(handleBrands)
	h = servertiming.Middleware(h, nil)
	http.Handle("/brands", h)
}

func handleBrands(w http.ResponseWriter, r *http.Request) {
	timing := servertiming.FromContext(r.Context())

	lang := getBrandsQueryParams(r.URL.Query())

	metric := timing.NewMetric("sql").WithDesc("SQL query").Start()
	brands := model.GetBrands(lang)
	metric.Stop()

	log.Println(brands)
	log.Println(metric)

	enableCors(&w)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(brands)
}

func getBrandsQueryParams(query url.Values) (lang string) {
	lang = query.Get("lang")
	if lang == "" {
		lang = "en"
	}

	return lang
}
