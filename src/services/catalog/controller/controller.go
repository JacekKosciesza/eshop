package controller

var (
	itemsController items
	brandsController brands
	typesController types
)

func Startup() {
	itemsController.registerRoutes()
	brandsController.registerRoutes()
	typesController.registerRoutes()
}
