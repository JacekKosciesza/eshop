package controller

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"strconv"

	servertiming "github.com/mitchellh/go-server-timing"
	"gitlab.com/JacekKosciesza/eshop/model"
)

type items struct{}

func (i items) registerRoutes() {
	var h http.Handler = http.HandlerFunc(handleItems)
	h = servertiming.Middleware(h, nil)
	http.Handle("/items", h)
}

func handleItems(w http.ResponseWriter, r *http.Request) {
	timing := servertiming.FromContext(r.Context())

	page, brandID, typeID, lang := getItemsQueryParams(r.URL.Query())

	metric := timing.NewMetric("sql").WithDesc("SQL query").Start()
	items := model.GetCatalog(lang, page, brandID, typeID)
	metric.Stop()

	log.Println(items)
	log.Println(metric)

	enableCors(&w)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(items)
}

func getItemsQueryParams(query url.Values) (page int, brandID sql.NullInt64, typeID sql.NullInt64, lang string) {
	pageParam := query.Get("page")
	if pageParam == "" {
		pageParam = "1"
	}
	page, _ = strconv.Atoi(pageParam)

	brandIDParam := query.Get("brandId")
	if brandIDParam != "" {
		brandID.Int64, _ = strconv.ParseInt(brandIDParam, 10, 64)
		brandID.Valid = true
	}

	typeIDParam := query.Get("typeId")
	if typeIDParam != "" {
		typeID.Int64, _ = strconv.ParseInt(typeIDParam, 10, 64)
		typeID.Valid = true
	}

	lang = query.Get("lang")
	if lang == "" {
		lang = "en"
	}

	return page, brandID, typeID, lang
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
