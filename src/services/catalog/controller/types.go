package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"

	servertiming "github.com/mitchellh/go-server-timing"
	"gitlab.com/JacekKosciesza/eshop/model"
)

type types struct{}

func (b types) registerRoutes() {
	var h http.Handler = http.HandlerFunc(handleTypes)
	h = servertiming.Middleware(h, nil)
	http.Handle("/types", h)
}

func handleTypes(w http.ResponseWriter, r *http.Request) {
	timing := servertiming.FromContext(r.Context())

	lang := getBrandsQueryParams(r.URL.Query())

	metric := timing.NewMetric("sql").WithDesc("SQL query").Start()
	types := model.GetTypes(lang)
	metric.Stop()

	log.Println(types)
	log.Println(metric)

	enableCors(&w)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(types)
}

func getTypesQueryParams(query url.Values) (lang string) {
	lang = query.Get("lang")
	if lang == "" {
		lang = "en"
	}

	return lang
}
