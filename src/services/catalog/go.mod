module gitlab.com/JacekKosciesza/eshop

go 1.12

require (
	github.com/lib/pq v1.2.0
	github.com/mitchellh/go-server-timing v1.0.0
)
