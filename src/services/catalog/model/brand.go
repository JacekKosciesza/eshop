package model

import (
	"log"
)

var brandsModel BrandsModel

type BrandsModel interface {
	GetBrands() []Brand
}

type Brand struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func GetBrands(lang string) []Brand {
	rows, err := db.Query(`
		SELECT brands.id, name FROM brands
		JOIN brands_translations on (brands.Id = brands_translations.Id)
		WHERE brands_translations.language = $1`,
		lang)
	logFatal(err)

	var brands []Brand

	for rows.Next() {
		brand := &Brand{}
		err = rows.Scan(&brand.ID, &brand.Name)
		logFatal(err)
		log.Println("brand", brand)
		brands = append(brands, *brand)
	}

	return brands
}
