package model

import (
	"database/sql"
	"log"
)

var itemsModel ItemsModel

type ItemsModel interface {
	GetCatalog() Catalog
	GetItem(itemID int) Item
}

type Catalog struct {
	Items          []Item         `json:"items"`
	PaginationInfo PaginationInfo `json:"paginationInfo"`
}

type Item struct {
	ID         int     `json:"id"`
	Name       string  `json:"name"`
	Price      float32 `json:"price"`
	PictureURI string  `json:"pictureUri"`
	BrandID    int     `json:"brandId"`
	TypeID     int     `json:"typeId"`
}

type PaginationInfo struct {
	ItemsPage  int `json:"itemsPage"`
	TotalItems int `json:"totalItems"`
	ActualPage int `json:"actualPage"`
	TotalPages int `json:"totalPages"`
	Items      int `json:"items"`
}

func GetCatalog(lang string, page int, brandID sql.NullInt64, typeID sql.NullInt64) Catalog {
	limit := 10
	offset := limit * (page - 1)

	rows, err := db.Query(`
		SELECT items."id", "name", price, items."pictureUri", items."brandId", items."typeId", count(*) OVER() AS total FROM items
		JOIN items_translations on (items.Id = items_translations.Id)
		WHERE (items_translations.language = $1)
		AND ("brandId" = $2 OR $2 IS NULL)
		AND ("typeId" = $3 OR $2 IS NULL)
		ORDER BY id
		LIMIT $4 OFFSET $5`,
		lang,
		brandID,
		typeID,
		limit, offset)
	logFatal(err)

	var items []Item
	var total int

	for rows.Next() {
		item := &Item{}
		err = rows.Scan(&item.ID, &item.Name, &item.Price, &item.PictureURI, &item.BrandID, &item.TypeID, &total)
		logFatal(err)
		log.Println("item", item)
		items = append(items, *item)
	}

	var paginationInfo PaginationInfo
	paginationInfo.ItemsPage = limit
	paginationInfo.TotalItems = total
	paginationInfo.ActualPage = page
	paginationInfo.TotalPages = (total / limit) + 1
	paginationInfo.Items = len(items)

	catalog := Catalog{items, paginationInfo}

	return catalog
}

func GetItem(itemID int) Item {
	row := db.QueryRow("SELECT * FROM items WHERE id = $1", itemID)
	item := &Item{}
	row.Scan(&item.ID, &item.Name, &item.Price, &item.PictureURI, &item.BrandID, &item.TypeID)
	log.Println("item", item)
	return *item
}

func UpdateItem(item Item) {
	_, err := db.Exec("UPDATE items SET name = $1, price = $2, PictureURI = $3 WHERE id = $4", item.Name, item.Price, item.PictureURI, item.ID)
	if err != nil {
		log.Printf("Failed to update '%v' item: %v", item.ID, err)
	}
}

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
