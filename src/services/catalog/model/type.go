package model

import (
	"log"
)

var typesModel TypesModel

type TypesModel interface {
	GetTypes() []Type
}

type Type struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func GetTypes(lang string) []Type {
	rows, err := db.Query(`
		SELECT types.id, name FROM types
		JOIN types_translations on (types.Id = types_translations.Id)
		WHERE types_translations.language = $1`,
		lang)
	logFatal(err)

	var types []Type

	for rows.Next() {
		_type := &Type{}
		err = rows.Scan(&_type.ID, &_type.Name)
		logFatal(err)
		log.Println("type", _type)
		types = append(types, *_type)
	}

	return types
}
