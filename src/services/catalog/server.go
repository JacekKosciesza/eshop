package main

import (
	"database/sql"
	"log"
	"net/http"
	"os"

	_ "github.com/lib/pq"
	"gitlab.com/JacekKosciesza/eshop/controller"
	"gitlab.com/JacekKosciesza/eshop/model"
)

func main() {
	db := connectToDatabase()
	defer db.Close()
	controller.Startup()
	log.Println("Starting Server")
	err := http.ListenAndServe(":82", nil)
	logFatal(err)
}

func connectToDatabase() *sql.DB {
	connStr := os.Getenv("ESHOP_DATABASE_URI")
	// log.Println(connStr)
	db, err := sql.Open("postgres", connStr)
	logFatal(err)
	model.SetDatabase(db)
	return db
}

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
