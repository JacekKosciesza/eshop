package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetCatalogReturnItems(t *testing.T) {
	catalog := getCatalog()
	if catalog == nil || len(catalog) <= 0 {
		t.Error("Nil or empty catalog")
	}
}

func TestCatalogHandlerReturnItems(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, "/", nil)
	w := httptest.NewRecorder()

	catalogHandler(w, r)

	actual, _ := ioutil.ReadAll(w.Result().Body)

	if actual == nil {
		t.Error("Nil result body")
	}
}
