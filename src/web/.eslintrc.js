module.exports = {
  extends: ['@open-wc/eslint-config', 'eslint-config-prettier', 'plugin:lit/recommended'],
  rules: {
    'linebreak-style': [0, 'error', 'windows'],
    'import/extensions': 'off',
    'class-methods-use-this': 0,
    'no-console': 0,
  },
};
