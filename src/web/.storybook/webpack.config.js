const createDefaultConfig = require('@open-wc/demoing-storybook/default-storybook-webpack-config.js');

const merge = require('webpack-merge');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = ({ config }) => {
  return merge(
    createDefaultConfig({ config, transpilePackages: ['lit-html', 'lit-element', '@open-wc'] }),
    {
      plugins: [new CopyWebpackPlugin([{ from: 'images', to: 'images', debug: true }])],
    },
  );
};
