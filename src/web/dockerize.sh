#!/bin/bash
npm run build
docker build  -f Dockerfile -t jkosciesza/eshop-web .
docker push jkosciesza/eshop-web

npm run site:build
docker build -t jkosciesza/eshop-storybook -f ./.storybook/Dockerfile .
docker push jkosciesza/eshop-storybook
