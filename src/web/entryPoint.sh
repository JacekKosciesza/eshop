#!/bin/sh

sed -i "s|__ESHOP_ENV__|$ESHOP_ENV|g" /usr/share/nginx/html/esh-app*.js
sed -i "s|__ESHOP_CATALOG_URI__|$ESHOP_CATALOG_URI|g" /usr/share/nginx/html/esh-catalog*.js
sed -i "s|__ESHOP_MOCK_DATA__|$ESHOP_MOCK_DATA|g" /usr/share/nginx/html/esh-catalog*.js

exec "$@"