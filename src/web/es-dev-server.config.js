module.exports = {
  port: 8080,
  watch: true,
  nodeResolve: true,
  http2: false,
  open: true,
  appIndex: 'index.html',
  moduleDirs: ['node_modules'],
  customMiddlewares: [
    async function headers(ctx, next) {
      ctx.set('Strict-Transport-Security', 'max-age=31556926; includeSubDomains; preload');
      ctx.set('Referrer-Policy', 'strict-origin-when-cross-origin');
      ctx.set('X-Frame-Options', 'DENY');
      ctx.set('X-Content-Type-Options', 'nosniff');
      ctx.set(
        'X-XSS-Protection',
        '1; mode=block; report=https://eshop.report-uri.com/r/d/xss/enforce',
      );
      // ctx.set(
      //   'Content-Security-Policy',
      //   "default-src 'none'; script-src 'self' https://storage.googleapis.com; style-src 'self' 'sha256-vd6mOz1RUFTauOQVtZlUlbVdFR16lDPymMtstq3/8I8='; object-src 'none'; img-src 'self'; manifest-src 'self'; connect-src 'self' https://catalog.eshop.wtf/; font-src 'self'; frame-src 'self'; form-action 'none'; base-uri 'none'; frame-ancestors 'none'; upgrade-insecure-requests; report-uri https://eshop.report-uri.com/r/d/csp/enforce",
      // );
      ctx.set('Cross-Origin-Opener-Policy', 'same-origin');
      ctx.set(
        'Feature-Policy',
        "geolocation 'none'; midi 'none'; sync-xhr 'none'; microphone 'none'; camera 'none'; magnetometer 'none'; gyroscope 'none'; speaker 'none'; fullscreen 'none'; payment 'none'",
      );
      ctx.set(
        'Expect-CT',
        'max-age=604800, report-uri=https://eshop.report-uri.com/r/d/ct/enforce',
      );
      await next();
    },
    async function replace(ctx, next) {
      await next();
      if (!ctx.body) {
        return;
      }
      if (ctx.url === '/index.html') {
        ctx.body = ctx.body.replace(
          '<link rel="preload" href="esh-catalog.js" as="script" crossorigin="anonymous" />',
          '',
        );
      }
      if (ctx.url === '/src/shared/esh-header.js' || ctx.url === '/src/shared/esh-footer.js') {
        ctx.body = ctx.body.replace('__ESHOP_ENV__', 'LOCAL');
      }
      if (ctx.url === '/src/catalog/esh-catalog.js') {
        const host = 'http://localhost:82';
        ctx.body = ctx.body.replace('__ESHOP_CATALOG_BRANDS_URI__', `${host}/brands`);
        ctx.body = ctx.body.replace('__ESHOP_CATALOG_TYPES_URI__', `${host}/types`);
        ctx.body = ctx.body.replace('__ESHOP_CATALOG_ITEMS_URI__', `${host}/items`);
        ctx.body = ctx.body.replace('__ESHOP_MOCK_DATA__', 'true');
      }
    },
  ],
};
