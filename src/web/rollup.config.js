import { createDefaultConfig } from '@open-wc/building-rollup';
import workbox from 'rollup-plugin-workbox-build';
import copy from 'rollup-plugin-copy';

const config = createDefaultConfig({
  input: './index.html',
  indexHTMLPlugin: {
    polyfills: {
      dynamicImport: false,
    },
    loader: 'external',
  },
});

export default {
  ...config,
  plugins: [
    ...config.plugins,
    copy({
      targets: [
        { src: 'images/*', dest: 'dist/images' },
        {
          src: [
            'favicon.ico',
            'robots.txt',
            'humans.txt',
            'manifest.json',
            'sitemap.xml',
            'budget.json',
          ],
          dest: 'dist',
        },
      ],
    }),
    workbox({
      mode: 'injectManifest',
      options: {
        swSrc: 'sw.template.js',
        swDest: 'dist/sw.js',
        globDirectory: 'dist',
        globPatterns: ['**/*.{js,css,html,webp,png,jpeg,jpg,ico,txt,xml,json}'],
      },
    }),
  ],
};
