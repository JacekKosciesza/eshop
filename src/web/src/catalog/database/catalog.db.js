import { openDB } from 'idb';

import { ITEMS } from '../data';

export class CatalogDb {
  static async initializeDb() {
    const VERSION = 1;

    const db = await openDB('eShop', VERSION, {
      upgrade(updateDb) {
        const store = updateDb.createObjectStore('catalog', {
          keyPath: 'id',
          autoIncrement: false,
        });
        store.createIndex('price', 'price');
      },
    });

    const tx = db.transaction('catalog', 'readwrite');
    try {
      ITEMS.forEach(item => {
        tx.store.add(item);
      });
      await tx.done;
    } catch (ex) {
      console.log('IndexedDB already populated with catalog items');
    }

    console.log(await db.getAllFromIndex('catalog', 'price'));
  }
}
