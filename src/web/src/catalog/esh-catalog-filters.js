import { LitElement, html, css } from 'lit-element';
import { translate } from '@appnest/lit-translate';

class EshCatalogFilters extends LitElement {
  static get properties() {
    return {
      errorReceived: { type: String },
      brands: { type: Array },
      brandId: { type: Number },
      types: { type: Array },
      typeId: { type: Number },
    };
  }

  constructor() {
    window.performance.mark('esh_catalog_filters_ctor');
    super();
    this.errorReceived = false;
    this.brands = [];
    this.brandId = 0;
    this.types = [];
    this.typeId = 0;
  }

  static get styles() {
    return [
      css`
        :host {
          display: flex;
          align-items: center;
          justify-content: center;

          background-color: var(--background-color, #00a69c);
          height: 65px;
        }

        .esh-catalog-filter {
          -webkit-appearance: none;
          background-color: transparent;
          border-color: var(--border-color, #00d9cc);
          color: var(--text-color, #ffffff);
          cursor: pointer;
          margin-right: 1rem;
          margin-top: 0.5rem;
          min-width: 140px;
          outline-color: var(--button-background-color, #83d01b);
          padding-bottom: 0;
          padding-left: 0.5rem;
          padding-right: 0.5rem;
          padding-top: 1.5rem;
        }

        .esh-catalog-filter option {
          background-color: var(--background-color, #00a69c);
        }

        .esh-catalog-label {
          display: inline-block;
          position: relative;
          z-index: 0;
        }

        .esh-catalog-label > span {
          color: var(--label-color, rgba(255, 255, 255, 0.5));
          content: attr(data-title);
          font-size: 0.65rem;
          margin-left: 0.5rem;
          margin-top: 0.65rem;
          position: absolute;
          text-transform: uppercase;
          z-index: 1;
        }

        .esh-catalog-label::after {
          content: '⏷';
          height: 7px;
          position: absolute;
          right: 1.6rem;
          top: 1.8rem;
          width: 10px;
          z-index: 1;
          color: var(--text-color, #ffffff);
        }

        .esh-catalog-send {
          border: 0;
          background-color: var(--button-background-color, #83d01b);
          color: var(--button-text-color, #ffffff);
          cursor: pointer;
          font-size: 2rem;
          padding: 0 0.6rem;
          transition: all 0.35s;
          margin-bottom: -6px;
        }

        .esh-catalog-send:hover {
          background-color: var(--button-background-color-hover, #4a760f);
          transition: all 0.35s;
        }
      `,
    ];
  }

  render() {
    return html`
      <div
        class="alert alert-warning esh-catalog-alert"
        role="alert"
        .hidden="${!this.errorReceived}"
      >
        Error requesting catalog products, please try later on
      </div>
      <label class="esh-catalog-label">
        <span>${translate('brand')}</span>
        <select class="esh-catalog-filter Brand" @change="${this.onBrandFilterChanged}">
          <option value="0" ?selected=${this.brandId === 0}>${translate('all')}</option>
          ${this.brands.map(
            brand => html`
              <option .value="${brand.id.toString()}" ?selected=${this.brandId === brand.id}
                >${brand.name}</option
              >
            `,
          )}
          ${this.isUknownOption(this.brands, this.brandId)
            ? html`
                <option ?selected=${this.isUknownOption(this.brands, this.brandId)}></option>
              `
            : html``}
          }
        </select>
      </label>
      <label class="esh-catalog-label">
        <span>${translate('type')}</span>
        <select class="esh-catalog-filter Type" @change="${this.onTypeFilterChanged}">
          <option value="0" ?selected=${this.typeId === 0}>${translate('all')}</option>
          ${this.types.map(
            type => html`
              <option .value="${type.id.toString()}" ?selected=${this.typeId === type.id}
                >${type.name}</option
              >
            `,
          )}
          ${this.isUknownOption(this.types, this.typeId)
            ? html`
                <option ?selected=${this.isUknownOption(this.types, this.typeId)}></option>
              `
            : html``}
          }
        </select>
      </label>
      <button class="esh-catalog-send" @click="${this.onFilterApplied}">
        ❯
      </button>
    `;
  }

  onBrandFilterChanged(e) {
    this.brandId = +e.target.value;
  }

  onTypeFilterChanged(e) {
    this.typeId = +e.target.value;
  }

  onFilterApplied() {
    const filterApplied = new CustomEvent('filterApplied', {
      detail: {
        brandId: this.brandId,
        typeId: this.typeId,
      },
    });
    this.dispatchEvent(filterApplied);
  }

  async firstUpdated() {
    window.performance.mark('esh_catalog_filters_firstUpdated');
  }

  async updated() {
    window.performance.mark('esh_catalog_filters_updated');
  }

  isUknownOption(options, selectedOption) {
    return ![0, ...options.map(b => b.id)].includes(+selectedOption);
  }
}

customElements.define('esh-catalog-filters', EshCatalogFilters);
