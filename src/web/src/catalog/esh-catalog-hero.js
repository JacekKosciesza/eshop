import { LitElement, html, css } from 'lit-element';
import { translate } from '@appnest/lit-translate';

class EshCatalogHero extends LitElement {
  static get styles() {
    return [
      css`
        :host {
          display: flex;
          align-items: center;
          background-color: var(--background-color, #ffffff);
          background-image: var(
            --background-image,
            url('/images/catalog/main_banner.4a44a2e7.jpg')
          );
          background-size: cover;
          height: 260px;
          width: 100%;
          filter: var(--image-filter);
        }

        .promo {
          margin-left: 10vw;
          border: 1px solid;
          border-color: var(--border-color, #808080);
          color: var(--text-color, #ffffff);
          text-transform: uppercase;
          width: 280px;
          height: 120px;
          font-size: 2.2rem;
          font-weight: bold;
        }

        .what {
          color: #86c440;
        }

        .when {
          color: #afe0e4;
          font-weight: normal;
        }
      `,
    ];
  }

  constructor() {
    window.performance.mark('esh_catalog_hero_ctor');
    super();
  }

  render() {
    return html`
      ${this.promoText}
    `;
  }

  get promoText() {
    return html`
      <div class="promo">
        <div>${translate('all')} <span class="what">${translate('tshirts')}</span></div>
        <div>${translate('onSale')}</div>
        <div class="when">${translate('thisWeekend')}</div>
      </div>
    `;
  }

  async firstUpdated() {
    window.performance.mark('esh_catalog_hero_firstUpdated');
  }

  async updated() {
    window.performance.mark('esh_catalog_hero_updated');
  }
}

customElements.define('esh-catalog-hero', EshCatalogHero);
