import { LitElement, html, css } from 'lit-element';
import { translate } from '@appnest/lit-translate';

class EshCatalogItem extends LitElement {
  static get properties() {
    return {
      item: { type: Object },
    };
  }

  constructor() {
    window.performance.mark('esh_catalog_item_ctor');
    super();
    this.item = {};
  }

  static get styles() {
    return [
      css`
        :host {
          background: var(--background-color, #ffffff);
          color: var(--text-color, #000000);

          display: flex;
          flex-direction: column;
          justify-content: start;
          align-items: center;
          margin: 5px;
        }

        @media screen and (max-width: 1024px) {
          :host {
            width: 50%;
          }
        }

        @media screen and (max-width: 768px) {
          :host {
            width: 100%;
          }
        }

        .esh-catalog-thumbnail {
          max-width: 370px;
          width: 100%;
          filter: var(--image-filter);
        }

        .esh-catalog-button {
          background-color: var(--button-background-color, #83d01b);
          border: 0;
          color: var(--button-text-color, #ffffff);
          cursor: pointer;
          font-size: 1rem;
          height: 3rem;
          margin-top: 1rem;
          transition: all 0.35s;
          width: 80%;
          text-transform: uppercase;
        }

        .esh-catalog-button.is-disabled {
          opacity: 0.5;
          pointer-events: none;
        }

        .esh-catalog-button:hover {
          background-color: var(--button-background-color-hover, #4a760f);
          transition: all 0.35s;
        }

        .esh-catalog-name {
          font-size: 1rem;
          font-weight: 300;
          margin-top: 0.5rem;
          text-align: center;
          text-transform: uppercase;
        }

        .esh-catalog-price {
          font-size: 28px;
          font-weight: 900;
          text-align: center;
        }
      `,
    ];
  }

  render() {
    return html`
      <picture class="esh-catalog-thumbnail">
        <source srcset="${this.item.pictureUri}.webp" type="image/webp" />
        <source srcset="${this.item.pictureUri}.jpg" type="image/jpeg" />
        <img
          src=${this.item.pictureUri}
          alt=${this.item.name}
          witdh="372"
          height="240"
          loading="lazy"
        />
      </picture>
      <button class="esh-catalog-button" @click=${this.addToCart}>
        [ ${translate('addToCart')} ]
      </button>

      <div class="esh-catalog-name">
        <span>${this.item.name}</span>
      </div>
      <div class="esh-catalog-price">
        <span>${translate('priceX', { price: this.item.price })}</span>
      </div>
    `;
  }

  addToCart() {
    // console.log('addToCart');
  }

  async firstUpdated() {
    window.performance.mark('esh_catalog_item_firstUpdated');
  }

  async updated() {
    window.performance.mark('esh_catalog_item_updated');
  }
}

customElements.define('esh-catalog-item', EshCatalogItem);
