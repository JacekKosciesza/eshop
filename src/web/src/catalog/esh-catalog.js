import { LitElement, html, css } from 'lit-element';

import './esh-catalog-hero';
import './esh-catalog-filters';
import './esh-pager';
import './esh-catalog-item';
import { BrandsMock, TypesMock, ItemsMock } from './mock';
import parseQueryString from '../shared/helpers/querystring.helper';

class EshCatalog extends LitElement {
  static get properties() {
    return {
      brands: { type: Array },
      brandId: { type: Number },
      types: { type: Array },
      typeId: { type: Number },
      items: { type: Array },
      paginationInfo: { type: Object },
      mockData: { type: Boolean },
      mockDelay: { type: Number },
      queryString: { type: String, reflect: true },
    };
  }

  constructor() {
    window.performance.mark('esh_catalog_ctor');
    super();
    this.updateFilter = this.updateFilter.bind(this);
    this.brands = [];
    this.brandId = 0;
    this.types = [];
    this.typeId = 0;
    this.items = [];
    this.paginationInfo = {};
    try {
      this.mockData = JSON.parse('__ESHOP_MOCK_DATA__');
      // eslint-disable-next-line no-empty
    } catch {}
    this.mockDelay = 0;
    // @ts-ignore
    // window.requestIdleCallback(() => {
    //   const marks = window.performance.getEntriesByType('mark');
    //   console.log(marks);

    //   [
    //     'app',
    //     'header',
    //     'footer',
    //     'catalog',
    //     'pager',
    //     'catalog_hero',
    //     'catalog_filters',
    //     'catalog_item',
    //   ].forEach(component => {
    //     window.performance.measure(
    //       `esh_${component}_firstUpdated`,
    //       `esh_${component}_ctor`,
    //       `esh_${component}_firstUpdated`,
    //     );
    //     window.performance.measure(
    //       `esh_${component}_updated`,
    //       `esh_${component}_ctor`,
    //       `esh_${component}_updated`,
    //     );
    //   });

    //   const measures = window.performance.getEntriesByType('measure');
    //   console.log(measures);
    // });
  }

  async firstUpdated() {
    window.performance.mark('esh_catalog_firstUpdated');
    await this.getBrands();
    await this.getTypes();
    // await this.getItems(window.location.search);
    // await CatalogDb.initializeDb();
  }

  async updated() {
    window.performance.mark('esh_catalog_updated');
  }

  async attributeChangedCallback(name, oldval, newval) {
    super.attributeChangedCallback(name, oldval, newval);
    if (name === 'querystring' && newval !== null) {
      await this.updateFilter(newval);
      await this.getItems(newval);
    }
  }

  updateFilter(queryString) {
    const { brandId, typeId } = parseQueryString(queryString);
    this.brandId = +(brandId || 0);
    this.typeId = +(typeId || 0);
  }

  async getItems(queryString) {
    const qs = queryString || '';
    let catalog;

    if (!this.mockData) {
      const url = `__ESHOP_CATALOG_ITEMS_URI__${qs}`;
      console.log(`fetch: ${url}`);
      const response = await fetch(url);
      catalog = await response.json();
    } else {
      console.log(`fetch: ItemsMock${qs}`);
      catalog = await ItemsMock.fetch(qs, this.mockDelay);
    }

    this.items = catalog.items || [];
    this.paginationInfo = catalog.paginationInfo;
  }

  async getBrands() {
    let brands;

    if (!this.mockData) {
      const url = '__ESHOP_CATALOG_BRANDS_URI__';
      console.log(`fetch: ${url}`);
      const response = await fetch(url);
      brands = await response.json();
    } else {
      brands = await BrandsMock.fetch();
    }
    this.brands = brands;
  }

  async getTypes() {
    let types;

    if (!this.mockData) {
      const url = '__ESHOP_CATALOG_TYPES_URI__';
      console.log(`fetch: ${url}`);
      const response = await fetch(url);
      types = await response.json();
    } else {
      types = await TypesMock.fetch();
    }
    this.types = types;
  }

  static get styles() {
    return [
      css`
        :host {
          display: flex;
          flex-direction: column;
          align-items: center;
          width: 100%;
          height: 100%;
        }

        esh-catalog-hero {
          --background-color: var(--theme-primary-background-color);
          --background-image: var(--theme-hero-background-image);
          --image-filter: var(--theme-image-filter);
        }

        esh-catalog-filters {
          width: 100%;
          --background-color: var(--theme-alternative-background-color);
          --text-color: var(--theme-secondary-text-color);
          --button-background-color: var(--theme-button-background-color);
          --button-background-color-hover: var(--theme-button-background-color-hover);
          --button-text-color: var(--theme-button-text-color);
          --label-color: var(--theme-label-color);
          --border-color: var(--theme-alternative-border-color);
        }

        esh-pager {
          margin: 30px 0;
          --background-color: var(--theme-primary-background-color);
          --text-color: var(--theme-primary-text-color);
          --button-background-color: var(--theme-button-background-color);
          --button-background-color-hover: var(--theme-button-background-color-hover);
          --button-text-color: var(--theme-button-text-color);
        }

        .esh-catalog-items {
          /* margin-top: 1rem; */
          display: flex;
          flex-direction: column;
          flex-flow: row wrap;
        }

        esh-catalog-item {
          --background-color: var(--theme-primary-background-color);
          --text-color: var(--theme-primary-text-color);
          --button-background-color: var(--theme-button-background-color);
          --button-background-color-hover: var(--theme-button-background-color-hover);
          --button-text-color: var(--theme-button-text-color);
          --image-filter: var(--theme-image-filter);
        }
      `,
    ];
  }

  render() {
    return html`
      <esh-catalog-hero></esh-catalog-hero>
      <esh-catalog-filters
        .brands=${this.brands}
        .brandId=${this.brandId}
        .types=${this.types}
        .typeId=${this.typeId}
        @filterApplied=${this.onFilterApplied}
      ></esh-catalog-filters>
      ${this.catalogTemplate}
    `;
  }

  get catalogTemplate() {
    return html`
      ${this.items.length
        ? html`
            <esh-pager .model=${this.paginationInfo} @changed=${this.onPageChanged}></esh-pager>
            <div class="esh-catalog-items">
              ${this.items.map(
                item =>
                  html`
                    <esh-catalog-item .item=${item}></esh-catalog-item>
                  `,
              )}
            </div>
            <esh-pager .model=${this.paginationInfo} @changed=${this.onPageChanged}></esh-pager>
          `
        : html`
            <div class="no-results">
              <span>THERE ARE NO RESULTS THAT MATCH YOUR SEARCH</span>
            </div>
          `}
    `;
  }

  async onFilterApplied(e) {
    const { brandId, typeId } = e.detail;
    this.brandId = brandId;
    this.typeId = typeId;

    this.paginationInfo.actualPage = 0;
    const queryString = this.buildQueryString(
      this.brandId,
      this.typeId,
      this.paginationInfo.actualPage,
    );
    this.navigate(queryString);
  }

  navigate(queryString) {
    const url = `/${queryString}`;
    window.history.pushState({}, '', url);
    const navigation = new CustomEvent('navigation', {
      detail: {
        location: window.location,
      },
    });
    this.dispatchEvent(navigation);
  }

  buildQueryString(brandId, typeId, page) {
    const queryParams = [];

    if (brandId) {
      queryParams.push(`brandId=${brandId}`);
    }

    if (typeId) {
      queryParams.push(`typeId=${typeId}`);
    }

    if (page > 1) {
      queryParams.push(`page=${page}`);
    }

    let queryString = `${queryParams.join('&')}`;
    queryString = queryString ? `?${queryString}` : ``;

    return queryString;
  }

  onPageChanged(e) {
    const value = e.detail.page;
    this.paginationInfo = {
      ...this.paginationInfo,
      actualPage: value,
    };
    const queryString = this.buildQueryString(
      this.brandId,
      this.typeId,
      this.paginationInfo.actualPage,
    );
    this.navigate(queryString);
  }

  addToCart() {
    // console.log('addToCart');
  }
}

customElements.define('esh-catalog', EshCatalog);
