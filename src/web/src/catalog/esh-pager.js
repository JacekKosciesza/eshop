import { LitElement, html, css } from 'lit-element';
import { translate } from '@appnest/lit-translate';

class EshPager extends LitElement {
  static get properties() {
    return {
      model: { type: Object },
      buttonStates: { type: Object },
    };
  }

  constructor() {
    window.performance.mark('esh_pager_ctor');
    super();
    this.model = {};
    this.buttonStates = {
      nextDisabled: true,
      previousDisabled: true,
    };
  }

  static get styles() {
    return [
      css`
        :host {
          display: flex;
          justify-content: space-between;
          align-items: center;
          background-color: var(--background-color, #ffffff);
          color: var(--text-color, #000000);
        }

        .esh-pager-item {
          margin: 0 5vw;
        }

        .esh-pager-item.is-disabled {
          opacity: 0;
          pointer-events: none;
        }

        .esh-pager-item--navigable {
          background-color: var(--button-background-color, #83d01b);
          color: var(--button-text-color, #ffffff);
          border: 0;
          cursor: pointer;
          padding: 5px;
        }

        .esh-pager-item--navigable:hover {
          background-color: var(--button-background-color-hover, #4a760f);
        }
      `,
    ];
  }

  render() {
    return html`
      <div class="esh-pager">
        <div class="container">
          <article class="esh-pager-wrapper row">
            <nav>
              <button
                id="Previous"
                class="esh-pager-item esh-pager-item--navigable ${this.buttonStates.previousDisabled
                  ? 'is-disabled'
                  : ''}"
                @click="${this.onPreviousClicked}"
                aria-label="Previous"
              >
                ${translate('previous')}
              </button>

              <span class="esh-pager-item">
                ${translate('showingXofYitems', {
                  x: () => this.model.items,
                  y: () => this.model.totalItems,
                })}
                - ${translate('page')} ${this.model.actualPage} - ${this.model.totalPages}
              </span>

              <button
                id="Next"
                class="esh-pager-item esh-pager-item--navigable ${this.buttonStates.nextDisabled
                  ? 'is-disabled'
                  : ''}"
                @click="${this.onNextClicked}"
                aria-label="Next"
              >
                ${translate('next')}
              </button>
            </nav>
          </article>
        </div>
      </div>
    `;
  }

  async firstUpdated() {
    window.performance.mark('esh_pager_firstUpdated');
  }

  updated(changedProperties) {
    window.performance.mark('esh_pager_updated');

    if (changedProperties && changedProperties.has('model')) {
      // this.model.items =
      //   this.model.itemsPage > this.model.totalItems ? this.model.totalItems : this.model.itemsPage;

      this.buttonStates.previousDisabled = this.model.actualPage === 1;
      this.buttonStates.nextDisabled = this.model.actualPage >= this.model.totalPages;
      this.requestUpdate();
    }
  }

  onPreviousClicked(event) {
    event.preventDefault();
    // console.log('Pager previous clicked');
    const changed = new CustomEvent('changed', {
      detail: {
        page: this.model.actualPage - 1,
      },
    });
    this.dispatchEvent(changed);
  }

  onNextClicked(event) {
    event.preventDefault();
    // console.log('Pager next clicked');
    const changed = new CustomEvent('changed', {
      detail: {
        page: this.model.actualPage + 1,
      },
    });
    this.dispatchEvent(changed);
  }
}

customElements.define('esh-pager', EshPager);
