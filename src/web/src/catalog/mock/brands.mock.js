import { BRANDS } from '../data';

export class BrandsMock {
  static fetch(delay) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(BRANDS);
      }, delay);
    });
  }
}
