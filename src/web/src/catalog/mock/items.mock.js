import { ITEMS } from '../data';
import parseQueryString from '../../shared/helpers/querystring.helper';

export class ItemsMock {
  static fetch(queryString, delay) {
    return new Promise(resolve => {
      setTimeout(() => {
        let filtered = ITEMS;

        const { brandId, typeId, page } = parseQueryString(queryString);
        const actualPage = +page || 1;

        if (brandId) {
          filtered = filtered.filter(i => i.brandId === +brandId);
        }

        if (typeId) {
          filtered = filtered.filter(i => i.typeId === +typeId);
        }

        const pageSize = 10;
        const currentPage = actualPage;
        const totalItems = filtered.length;
        const startIndex = (currentPage - 1) * pageSize;
        const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
        filtered = filtered.slice(startIndex, endIndex + 1);

        const items = filtered;
        const paginationInfo = {
          itemsPage: pageSize,
          totalItems,
          actualPage: currentPage,
          totalPages: Math.ceil(totalItems / pageSize),
          items: filtered.length,
        };

        resolve({
          items,
          paginationInfo,
        });
      }, delay);
    });
  }
}
