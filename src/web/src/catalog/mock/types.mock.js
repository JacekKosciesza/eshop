import { TYPES } from '../data';

export class TypesMock {
  static fetch(delay) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(TYPES);
      }, delay);
    });
  }
}
