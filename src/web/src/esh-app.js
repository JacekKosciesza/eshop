import { LitElement, html, css } from 'lit-element';
import { installRouter } from 'pwa-helpers/router.js';
import { registerTranslateConfig, use } from '@appnest/lit-translate';

import loader from './shared/i18n';
import './shared/esh-header';
import './shared/esh-footer';

import { theme } from './themes/all';

class EshApp extends LitElement {
  static get properties() {
    return {
      _page: { type: String },
      disabled: {
        type: Boolean,
        reflect: true,
      },
      queryString: { type: String },
    };
  }

  static get styles() {
    return [
      theme,
      css`
        :host {
          color: var(--theme-primary-text-color, #000000);
          background: var(--theme-primary-background-color, #ededed);
          display: grid;
          grid-template-columns: 1fr;
          min-height: 100vh;
          grid-template-areas:
            'header'
            'main'
            'footer';
          grid-template-rows: 100px 1fr 150px;
        }

        esh-header {
          grid-area: header;
          --background-color: var(--theme-primary-background-color);
          --text-color: var(--theme-primary-text-color);
          --border-color: var(--theme-border-color);
          --image-filter: var(--theme-image-filter);
        }

        main {
          grid-area: main;
        }

        .page {
          display: none;
        }
        .page[active] {
          display: flex;
        }

        esh-footer {
          grid-area: footer;
          --background-color: var(--theme-secondary-background-color);
          --border-color: var(--theme-border-color);
          --logo-text-color: var(--theme-secondary-text-color);
          --copyright-text-color: var(--theme-alternative-text-color);
        }
      `,
    ];
  }

  constructor() {
    window.performance.mark('esh_app_ctor');
    super();
    this.disabled = false;
    console.log('eShop v0.1.3');
    this._lang = 'en';
    this.registerServiceWorker();
    this.updateOnlineStatus = this.updateOnlineStatus.bind(this);
    this.updateOfflineStatus = this.updateOfflineStatus.bind(this);
    this.registerOnlineOfflineStatusEvents();
    this.queryString = window.location.search;
    registerTranslateConfig({ loader });
  }

  render() {
    return html`
      <esh-header></esh-header>
      <main role="main">
        <esh-catalog
          class="page"
          ?active="${this._page === 'catalog'}"
          .queryString=${this.queryString}
          @navigation=${this.onNavigation}
        ></esh-catalog>
        <esh-settings
          class="page"
          ?active="${this._page === 'settings'}"
          @themeChanged=${this.onThemeChanged}
          .lang=${this._lang}
        ></esh-settings>
      </main>
      <esh-footer></esh-footer>
    `;
  }

  registerServiceWorker() {
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker
          .register('/sw.js')
          .then(() => {
            console.log('Service Worker registered! 😎');
          })
          .catch(err => {
            console.log('Registration failed 😫 ', err);
          });
      });
    }
  }

  registerOnlineOfflineStatusEvents() {
    window.addEventListener('online', this.updateOnlineStatus);
    window.addEventListener('offline', this.updateOfflineStatus);
  }

  updateOnlineStatus() {
    console.log('Online');
    this.classList.remove('offline');
  }

  updateOfflineStatus() {
    console.log('Offline');
    this.classList.add('offline');
  }

  async firstUpdated() {
    window.performance.mark('esh_app_firstUpdated');
    await use(this._lang);
    installRouter((location, event) => {
      if (event && event.type === 'click') {
        window.scrollTo(0, 0);
      }
      this.handleNavigation(location);
    });
  }

  async updated() {
    window.performance.mark('esh_app_updated');
  }

  onNavigation(e) {
    this.handleNavigation(e.detail.location);
  }

  handleNavigation(location) {
    // @ts-ignore
    const path = window.decodeURIComponent(location.pathname);
    const page = path === '/' ? 'catalog' : path.slice(1);
    this._loadPage(page);
  }

  _loadPage(page) {
    switch (page) {
      case 'catalog':
        import('./catalog/esh-catalog').then(() => {
          this.queryString = window.location.search;
        });
        break;
      case 'settings':
        import('./settings/esh-settings');
        break;
      default:
        break;
    }

    this._page = page;
  }

  onThemeChanged(e) {
    const themeClass = e.detail.theme;
    this.className = '';
    if (themeClass !== 'default') {
      this.classList.add(themeClass);
    }
  }
}

customElements.define('esh-app', EshApp);
