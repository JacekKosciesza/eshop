import { LitElement, html, css } from 'lit-element';
import { translate, use } from '@appnest/lit-translate';

class EshSettings extends LitElement {
  static get properties() {
    return {
      lang: { type: String },
    };
  }

  static get styles() {
    return [
      css`
        :host {
          display: block;
        }
      `,
    ];
  }

  constructor() {
    window.performance.mark('esh_settings_ctor');
    super();
  }

  render() {
    return html`
      <h1>${translate('settings')}</h1>

      <div>
        <label>
          <span>${translate('theme')}</span>
          <select @change="${this.onThemeChanged}">
            <option value="default">${translate('default')}</option>
            <option value="contrast">${translate('contrast')}</option>
          </select>
        </label>
      </div>

      <div>
        <label>
          <span>${translate('language')}</span>
          <select @change="${this.onLanguageChanged}">
            <option value="en" ?selected=${this.lang === 'en'}>English</option>
            <option value="pl" ?selected=${this.lang === 'pl'}>Polski</option>
          </select>
        </label>
      </div>
    `;
  }

  onThemeChanged(e) {
    const themeChanged = new CustomEvent('themeChanged', {
      detail: {
        theme: e.target.value,
      },
    });
    this.dispatchEvent(themeChanged);
  }

  onLanguageChanged(e) {
    const lang = e.target.value;
    this.lang = lang;
    use(lang);
  }
}

customElements.define('esh-settings', EshSettings);
