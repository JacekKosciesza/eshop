import { LitElement, html, css } from 'lit-element';

class EshFooter extends LitElement {
  static get properties() {
    return {
      env: { type: String },
    };
  }

  constructor() {
    window.performance.mark('esh_footer_ctor');
    super();
    this.env = '__ESHOP_ENV__';
  }

  static get styles() {
    return [
      css`
        :host {
          background-color: var(--background-color, black);

          border-top: 1px solid #eee;
          padding-left: 50px;
          padding-right: 50px;

          display: flex;
          flex-direction: row;
          flex-wrap: wrap;
          justify-content: space-between;
          align-items: center;
        }

        :host > div {
          border: 1px solid;
          border-color: var(--border-color, #808080);
        }

        .logo {
          display: flex;
          align-items: center;
          color: var(--logo-text-color, #ffffff);
        }

        .logotype {
          margin-left: 10px;
          font-size: 1.4rem;
        }

        .logotype .env {
          color: #aaa;
          font-size: 0.6em;
        }

        .copyright {
          color: var(--copyright-text-color, #86b435);
          font-weight: bold;
          padding: 3px 10px;
        }
      `,
    ];
  }

  render() {
    return html`
      <div class="logo">
        <svg width="50px" height="50px" viewBox="0 0 82 82">
          <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g>
              <g fill="#fff">
                <rect x="0" y="78" width="20" height="4"></rect>
                <rect x="0" y="0" width="20" height="4"></rect>
                <rect x="0" y="0" width="4" height="82"></rect>
              </g>
              <g
                transform="translate(72.000000, 41.000000) scale(-1, 1) translate(-72.000000, -41.000000) translate(62.000000, 0.000000)"
                fill="#fff"
              >
                <rect x="0" y="78" width="20" height="4"></rect>
                <rect x="0" y="0" width="20" height="4"></rect>
                <rect x="0" y="0" width="4" height="82"></rect>
              </g>
              <path
                d="M55.2,35.44 C55.2,33.5999908 54.9400026,31.8400084 54.42,30.16 C53.8999974,28.4799916 53.0800056,27.0000064 51.96,25.72 C50.8399944,24.4399936 49.4200086,23.4200038 47.7,22.66 C45.9799914,21.8999962 43.9600116,21.52 41.64,21.52 C37.3199784,21.52 33.660015,22.819987 30.66,25.42 C27.659985,28.020013 26.0000016,31.3599796 25.68,35.44 L55.2,35.44 Z M69.6,41.92 L69.6,43.84 C69.6,44.4800032 69.5600004,45.1199968 69.48,45.76 L25.68,45.76 C25.8400008,47.8400104 26.3799954,49.7399914 27.3,51.46 C28.2200046,53.1800086 29.4399924,54.6599938 30.96,55.9 C32.4800076,57.1400062 34.1999904,58.1199964 36.12,58.84 C38.0400096,59.5600036 40.0399896,59.92 42.12,59.92 C45.720018,59.92 48.7599876,59.2600066 51.24,57.94 C53.7200124,56.6199934 55.759992,54.8000116 57.36,52.48 L66.96,60.16 C61.2799716,67.8400384 53.040054,71.68 42.24,71.68 C37.7599776,71.68 33.6400188,70.980007 29.88,69.58 C26.1199812,68.179993 22.8600138,66.2000128 20.1,63.64 C17.3399862,61.0799872 15.1800078,57.9400186 13.62,54.22 C12.0599922,50.4999814 11.28,46.2800236 11.28,41.56 C11.28,36.9199768 12.0599922,32.700019 13.62,28.9 C15.1800078,25.099981 17.3199864,21.8600134 20.04,19.18 C22.7600136,16.4999866 25.9799814,14.4200074 29.7,12.94 C33.4200186,11.4599926 37.4399784,10.72 41.76,10.72 C45.76002,10.72 49.459983,11.3799934 52.86,12.7 C56.260017,14.0200066 59.1999876,15.979987 61.68,18.58 C64.1600124,21.180013 66.099993,24.4199806 67.5,28.3 C68.900007,32.1800194 69.6,36.719974 69.6,41.92 Z"
                id="e"
                fill="#fff"
                fill-rule="nonzero"
                transform="translate(40.440000, 41.200000) rotate(-7.000000) translate(-40.440000, -41.200000) "
              ></path>
            </g>
          </g>
        </svg>
        <div class="logotype">
          <div>
            eSHOP
            ${this.env
              ? html`
                  <sup class="env">[${this.env}]</sup>
                `
              : html``}
          </div>
          <div>onCONTAINERS</div>
        </div>
      </div>
      <div class="copyright">e-ShopOnContainers. By Code & Pepper</div>
    `;
  }

  async firstUpdated() {
    window.performance.mark('esh_footer_firstUpdated');
  }

  async updated() {
    window.performance.mark('esh_footer_updated');
  }
}

customElements.define('esh-footer', EshFooter);
