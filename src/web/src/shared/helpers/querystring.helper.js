export default function parseQueryString(queryString) {
  if (!queryString) {
    return {};
  }

  return JSON.parse(
    `{"${decodeURI(queryString.substring(1))
      .replace(/"/g, '\\"')
      .replace(/&/g, '","')
      .replace(/=/g, '":"')}"}`,
  );
}
