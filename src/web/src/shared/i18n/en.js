export default {
  settings: 'Settings',
  language: 'Language',
  theme: 'Theme',
  default: 'Default',
  contrast: 'Contrast',
  local: 'Local',
  // esh-catalog-filter
  brand: 'Brand',
  type: 'Type',
  all: 'All',
  // esh-pager
  previous: 'Previous',
  showingXofYitems: 'Showing {{ x }} of {{ y }} items',
  page: 'Page',
  next: 'Next',
  // esh-catalog-item
  addToCart: 'Add to cart',
  // eslint-disable-next-line no-template-curly-in-string
  priceX: '${{ price }}',
  // esh-catalog-hero
  tshirts: 't-shirts',
  onSale: 'on sale',
  thisWeekend: 'this weekend',
};
