import pl from './pl';
import en from './en';

// https://github.com/andreasbm/lit-translate
export default lang =>
  new Promise(resolve => {
    switch (lang) {
      case 'pl':
        resolve(pl);
        break;
      default:
        resolve(en);
        break;
    }
  });
