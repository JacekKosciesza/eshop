export default {
  settings: 'Ustawienia',
  language: 'Język',
  theme: 'Motyw',
  default: 'Domyślny',
  contrast: 'Kontrastowy',
  local: 'Lokalny',
  // esh-catalog-filter
  brand: 'Marka',
  type: 'Typ',
  all: 'Wszystkie',
  // esh-pager
  previous: 'Poprzednia',
  showingXofYitems: 'Wyświetlone {{ x }} z {{ y }} produktów',
  page: 'Strona',
  next: 'Następna',
  // esh-catalog-item
  addToCart: 'Dodaj do koszyka',
  priceX: '{{ price }} zł',
  // esh-catalog-hero
  tshirts: 'koszulki',
  onSale: 'na wyprzedaży',
  thisWeekend: 'w ten weekend',
};
