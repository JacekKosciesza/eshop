import { css } from 'lit-element';

export const theme = css`
  :host {
    --theme-primary-background-color: #ededed;
    --theme-primary-text-color: #000000;

    --theme-secondary-background-color: #000000;
    --theme-secondary-text-color: #ffffff;

    --theme-alternative-background-color: #00857c; /*00a69c*/
    --theme-alternative-text-color: #86b435;

    --theme-border-color: #808080;
    --theme-alternative-border-color: #00d9cc;
    --theme-label-color: rgba(255, 255, 255, 1); /*rgba(255, 255, 255, 0.5)*/

    --theme-hero-background-image: url('/images/catalog/main_banner.4a44a2e7.jpg');

    --theme-button-background-color: #466e11; /*#83d01b*/
    --theme-button-background-color-hover: #4a760f;
    --theme-button-color: #ffffff;
  }

  :host-context(.contrast) {
    /*
    #fffe51; yellow
    #79ed5c; green
    #79ed5c; blue
    #eb3ff2; purple
    */

    --theme-primary-background-color: #000000;
    --theme-primary-text-color: #ffffff;

    --theme-secondary-background-color: #000000;
    --theme-secondary-text-color: #ffffff;

    --theme-alternative-background-color: #000000;
    --theme-alternative-text-color: #86b435;

    --theme-border-color: #808080;
    --theme-alternative-border-color: #ffffff;
    --theme-label-color: #ffffff;

    --theme-hero-background-image: url('/images/catalog/main_banner.4a44a2e7.jpg');

    --theme-button-background-color: #eb3ff2;
    --theme-button-background-color-hover: #8b0a8f;
    --theme-button-text-color: #000000;
  }

  :host-context(.offline) {
    --theme-primary-background-color: #ededed;
    --theme-primary-text-color: #000000;

    --theme-secondary-background-color: #000000;
    --theme-secondary-text-color: #ffffff;

    --theme-alternative-background-color: #686868;
    --theme-alternative-text-color: #a0a0a0;

    --theme-border-color: #808080;
    --theme-alternative-border-color: #ababab;
    --theme-label-color: rgba(255, 255, 255, 1);

    --theme-hero-background-image: url('/images/catalog/main_banner.4a44a2e7.jpg');
    --theme-image-filter: grayscale(100%);

    --theme-button-background-color: #656565;
    --theme-button-background-color-hover: #5f5f5f;
    --theme-button-color: #ffffff;
  }
`;
