import { storiesOf, html } from '@open-wc/demoing-storybook';

import '../../../src/catalog/esh-catalog-filters';

storiesOf('Demo|Catalog', module).add(
  'Filters (esh-catalog-filters)',
  () => html`
    <esh-catalog-filters></esh-catalog-filters>
  `,
);
