import { storiesOf, html } from '@open-wc/demoing-storybook';

import '../../../src/catalog/esh-catalog-hero';

storiesOf('Demo|Catalog', module).add(
  'Hero (esh-catalog-hero)',
  () => html`
    <esh-catalog-hero></esh-catalog-hero>
  `,
);
