import { storiesOf, html } from '@open-wc/demoing-storybook';

import '../../../src/catalog/esh-catalog-item';

const item = {
  name: '.NET BLACK & WHITE MUG',
  price: 8.5,
  pictureUri: '/images/catalog/items/2',
};

storiesOf('Demo|Catalog', module).add(
  'Item (esh-catalog-item)',
  () => html`
    <esh-catalog-item .item=${item}></esh-catalog-item>
  `,
);
