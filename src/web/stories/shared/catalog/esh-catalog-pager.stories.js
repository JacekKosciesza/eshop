import { storiesOf, html } from '@open-wc/demoing-storybook';

import '../../../src/catalog/esh-pager';

const paginationInfo = {
  itemsPage: 10,
  totalItems: 12,
  actualPage: 0,
  totalPages: 2,
  items: 10,
};

storiesOf('Demo|Catalog', module).add(
  'Pager (esh-pager)',
  () => html`
    <esh-pager .model=${paginationInfo}></esh-pager>
  `,
);
