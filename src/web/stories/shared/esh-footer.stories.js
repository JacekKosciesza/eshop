import { storiesOf, html } from '@open-wc/demoing-storybook';

import '../../src/shared/esh-footer';

storiesOf('Demo|App Shell', module).add(
  'Footer (esh-footer)',
  () => html`
    <esh-footer></esh-footer>
  `,
);
