import { storiesOf, html } from '@open-wc/demoing-storybook';

import '../../src/shared/esh-header';

storiesOf('Demo|App Shell', module).add(
  'Header (esh-header)',
  () => html`
    <esh-header></esh-header>
  `,
);
