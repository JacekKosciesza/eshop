/* eslint-disable no-undef */
// import { precacheAndRoute } from 'workbox-precaching/precacheAndRoute.mjs';
importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

workbox.precaching.precacheAndRoute([
  {
    url: 'index.html',
  },
  {
    url: 'src/esh-app.js',
  },
  {
    url: 'manifest.json',
  },
  {
    url: 'favicon.ico',
  },
  {
    url: 'node_modules/lit-element/lit-element.js',
  },
  {
    url: 'node_modules/pwa-helpers/router.js',
  },
  {
    url: 'src/shared/esh-header.js',
  },
  {
    url: 'src/shared/esh-footer.js',
  },
]);
