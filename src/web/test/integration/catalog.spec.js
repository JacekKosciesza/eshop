// @ts-check

const puppeteer = require('puppeteer');
const chai = require('chai');
chai.use(require('chai-string'));

describe('catalog', async () => {
  let browser;
  let context;
  let page;

  before(async () => {
    browser = await puppeteer.launch({
      headless: false,
      ignoreHTTPSErrors: true,
    });
  });

  beforeEach(async () => {
    context = await browser.createIncognitoBrowserContext();
    page = await context.newPage();
  });

  it('promo loaded', async () => {
    await page.goto('http://localhost:8080');

    await page.waitFor(1000);

    await page.waitForSelector('esh-app');

    await page.waitForFunction(() =>
      document.querySelector('esh-app').shadowRoot.querySelector('esh-catalog'),
    );

    await page.waitForFunction(() =>
      document
        .querySelector('esh-app')
        .shadowRoot.querySelector('esh-catalog')
        .shadowRoot.querySelector('esh-catalog-hero'),
    );

    const text = await page.evaluate(
      () =>
        document
          .querySelector('esh-app')
          .shadowRoot.querySelector('esh-catalog')
          .shadowRoot.querySelector('esh-catalog-hero').shadowRoot.textContent,
    );

    // @ts-ignore
    chai.expect(text).to.equalIgnoreSpaces(`All t-shirts on sale this weekend`);
  });

  afterEach(async () => {
    await context.close();
  });

  after(async () => {
    await browser.close();
  });
});
