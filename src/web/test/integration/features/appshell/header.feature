Feature: Header
    In order to easily navigate between pages
    As a user
    I want to be able to use links in the header

    Scenario: Logo link
        Given I go to "settings" page
        When I click "logo" link
        Then I see "/" page

    Scenario: Settings link
        Given I go to "catalog" page
        When I click "settings" link
        Then I see "settings" page

    Scenario: Browser history buttons
        Given I go to "catalog" page
        When I click "settings" link
        And I click browser's "back" button
        Then I see "catalog" page
        When I click browser's "forward" button
        Then I see "settings" page