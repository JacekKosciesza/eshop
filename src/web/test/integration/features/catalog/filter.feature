
Feature: Filter
  In order to easily find what I am looking for in catalog
  As a user
  I want to be able to filter catalog items by brand and type

  Scenario: Default filter
    When I go to "catalog" page
    Then I see "All" selected in "Brand" dropdown
    And I see "All" selected in "Type" dropdown

  Scenario: Filter in URL
    When I go to "catalog?brandId=1&typeId=1" page
    Then I see ".NET" selected in "Brand" dropdown
    And I see "Mug" selected in "Type" dropdown

  Scenario: Non existing filters
    When I go to "catalog?brandId=999&typeId=666" page
    Then I see "" selected in "Brand" dropdown
    And I see "" selected in "Type" dropdown

  Scenario Outline: Brand filter
    Given I go to "catalog" page
    And I see 1 of 2 pages
    And I see 10 of 12 items
    When I change "Brand" to "<brand>"
    And I click apply button
    And I wait 1000 milliseconds
    Then I see <pages> of <totalPages> pages
    And I see <items> of <totalItems> items
    Examples:
      | brand | pages | totalPages | items | totalItems |
      | .NET  | 1     | 1          | 5     | 5          |
      | Other | 1     | 1          | 7     | 7          |

  Scenario: Type filter
    Given I go to "catalog" page
    And I see 1 of 2 pages
    And I see 10 of 12 items
    When I change "Type" to 'Mug'
    And I click apply button
    And I wait 1000 milliseconds
    Then I see 1 of 1 pages
    And I see 2 of 2 items

  Scenario: Both filters
    Given I go to "catalog" page
    And I see 1 of 2 pages
    And I see 10 of 12 items
    When I change "Brand" to '.NET'
    And I change "Type" to 'Mug'
    And I click apply button
    And I wait 1000 milliseconds
    Then I see 1 of 1 pages
    And I see 1 of 1 items

  Scenario: Browser history
    Given I go to "catalog" page
    And I change "Brand" to '.NET'
    And I click apply button
    And I change "Type" to 'Mug'
    And I click apply button
    When I click browser's "back" button
    And I wait 1000 milliseconds
    Then I see 1 of 1 pages
    And I see 5 of 5 items
    When I click browser's "forward" button
    And I wait 1000 milliseconds
    Then I see 1 of 1 pages
    And I see 1 of 1 items

  Scenario: Reset page when filter applied
    Given I go to "catalog?page=2" page
    And I change "Type" to 'Mug'
    And I click apply button
    And I wait 1000 milliseconds
    Then I see 1 of 1 pages
    And I see 2 of 2 items
    And I don't see "Previous" button
    And I don't see "Next" button