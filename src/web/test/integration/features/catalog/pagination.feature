
Feature: Pagination
  In order to see all items from the catalog
  As a user
  I want to be able to navigate through catalog pages

  Scenario: Default page
    When I go to "catalog" page
    Then I see paginator
    And I don't see "Previous" button
    And I see 10 of 12 items
    And I see 1 of 2 pages
    And I see "Next" button

  Scenario: Page in URL
    When I go to "catalog?page=2" page
    Then I see paginator
    Then I see "Previous" button
    And I see 2 of 12 items
    And I see 2 of 2 pages
    And I don't see "Next" button

  Scenario: Non existing page
    When I go to "catalog?page=3" page
    Then I don't see paginator
    And I see message "THERE ARE NO RESULTS THAT MATCH YOUR SEARCH"

  Scenario: Next button
    Given I go to "catalog" page
    And I see 1 of 2 pages
    And I see 10 of 12 items
    When I click "Next" button
    And I wait 200 milliseconds
    Then I see 2 of 2 pages
    And I see 2 of 12 items

  Scenario: Previous button
    Given I go to "catalog?page=2" page
    And I see 2 of 2 pages
    And I see 2 of 12 items
    When I click "Previous" button
    And I wait 200 milliseconds
    Then I see 1 of 2 pages
    And I see 10 of 12 items

  Scenario: Browser history back button
    Given I go to "catalog" page
    When I click "Next" button
    And I wait 1000 milliseconds
    Then I see 2 of 2 pages
    And I see 2 of 12 items
    When I click browser's "back" button
    And I wait 1000 milliseconds
    Then I see 1 of 2 pages
    And I see 10 of 12 items

  Scenario: Browser history forward button
    Given I go to "catalog" page
    And I click "Next" button
    And I wait 1000 milliseconds
    And I click browser's "back" button
    And I wait 1000 milliseconds
    And I see 1 of 2 pages
    And I see 10 of 12 items
    When I click browser's "forward" button
    And I wait 1000 milliseconds
    Then I see 2 of 2 pages
    And I see 2 of 12 items