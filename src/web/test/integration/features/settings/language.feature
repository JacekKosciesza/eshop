Feature: Language
    In order to use application in different language
    As a user
    I want to be able to select langauge from a dropdown
    And I want to see languge change on the fly

  Scenario: Default language
    When I go to "settings" page
    Then I see header with "Settings" link

  Scenario: Change language
    Given I go to "settings" page
    When I change languge to "pl"
    Then I see header with "Ustawienia" link

  Scenario: Catalog page translated
    Given I go to "catalog" page
    And I see paginator text "Showing 10 of 12 items - Page 1 - 2"
    Given I go to "settings" page
    When I change languge to "pl"
    And I click "logo" link
    Then I see paginator text "Wyświetlone 10 z 12 produktów - Strona 1 - 2"