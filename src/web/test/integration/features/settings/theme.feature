Feature: Theme
    In order to use different theme
    As a user
    I want to be able to select theme from a dropdown
    And I want to see theme change on the fly

  Scenario: Default theme
    When I go to "settings" page
    Then I see header which has "rgb(237, 237, 237)" background color

  Scenario: Change theme
    Given I go to "settings" page
    When I change theme to "contrast"
    Then I see header which has "rgb(0, 0, 0)" background color