const { When } = require('cucumber');
const scope = require('../../support/scope');

When('I click {string} link', async link => {
  const { page } = scope;

  await page.evaluate(className => {
    const eshHeader = document.querySelector('esh-app').shadowRoot.querySelector('esh-header')
      .shadowRoot;

    /**
     * @type HTMLElement
     */
    const a = eshHeader.querySelector(`a.${className}`);

    a.click();
  }, link);

  await scope.page.waitFor(200); // TODO: do it better
});
