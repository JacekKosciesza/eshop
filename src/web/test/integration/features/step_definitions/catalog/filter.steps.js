const { When, Then } = require('cucumber');
const chai = require('chai');
chai.use(require('chai-string'));
const scope = require('../../support/scope');

Then('I see {string} selected in {string} dropdown', async (expectedOption, dropdown) => {
  const option = await scope.page.evaluate(selectClass => {
    const eshFilter = document
      .querySelector('esh-app')
      .shadowRoot.querySelector('esh-catalog')
      .shadowRoot.querySelector('esh-catalog-filters').shadowRoot;

    /**
     * @type HTMLSelectElement
     */
    const select = eshFilter.querySelector(`select.${selectClass}`);
    return select.options[select.selectedIndex].text;
  }, dropdown);

  chai.expect(option).to.be.equal(expectedOption);
});

When('I change {string} to {string}', async (dropdown, brandName) => {
  await scope.page.evaluate(
    (selectClass, optionText) => {
      const eshFilter = document
        .querySelector('esh-app')
        .shadowRoot.querySelector('esh-catalog')
        .shadowRoot.querySelector('esh-catalog-filters').shadowRoot;

      /**
       * @type HTMLSelectElement
       */
      const select = eshFilter.querySelector(`select.${selectClass}`);

      // @ts-ignore
      const option = [...select.querySelectorAll(`option`)].filter(
        o => o.innerText === optionText,
      )[0];
      option.selected = true;

      const event = new Event('change', { bubbles: true });
      select.dispatchEvent(event);
    },
    dropdown,
    brandName,
  );
});

When('I click apply button', async () => {
  await scope.page.evaluate(() => {
    const eshFilter = document
      .querySelector('esh-app')
      .shadowRoot.querySelector('esh-catalog')
      .shadowRoot.querySelector('esh-catalog-filters').shadowRoot;

    const button = eshFilter.querySelector(`button`);

    button.click();
  });
});
