const { When, Then } = require('cucumber');
const chai = require('chai');
chai.use(require('chai-string'));
const scope = require('../../support/scope');

const getPagerText = async () => {
  const text = await scope.page.evaluate(
    () =>
      document
        .querySelector('esh-app')
        .shadowRoot.querySelector('esh-catalog')
        .shadowRoot.querySelector('esh-pager')
        .shadowRoot.querySelector('span.esh-pager-item').textContent,
  );

  return text;
};

const getPagerValues = async () => {
  const text = await getPagerText();
  const pattern = /Showing\s(\d+)\s+of\s+(\d+)\s+items\s+-\s+Page\s+(\d+)\s+-\s+(\d+)/;
  const [, items, totalItems, page, totalPages] = pattern.exec(text);

  return { items: +items, totalItems: +totalItems, page: +page, totalPages: +totalPages };
};

const clickButtion = async type => {
  const opacity = await scope.page.evaluate(id => {
    const eshPager = document
      .querySelector('esh-app')
      .shadowRoot.querySelector('esh-catalog')
      .shadowRoot.querySelector('esh-pager').shadowRoot;

    /**
     * @type HTMLElement
     */
    const button = eshPager.querySelector(`#${id}`);

    button.click();
  }, type);

  return opacity;
};

const getButtonOpacity = async type => {
  const opacity = await scope.page.evaluate(id => {
    const eshPager = document
      .querySelector('esh-app')
      .shadowRoot.querySelector('esh-catalog')
      .shadowRoot.querySelector('esh-pager').shadowRoot;

    const button = eshPager.querySelector(`#${id}`);

    return getComputedStyle(button).getPropertyValue('opacity');
  }, type);

  return opacity;
};

const getPaginator = async () => {
  const eshPager = await scope.page.evaluate(() => {
    const eshCatalog = document.querySelector('esh-app').shadowRoot.querySelector('esh-catalog')
      .shadowRoot;

    const eshPaginator = eshCatalog.querySelector(`esh-pager`);

    return eshPaginator;
  });

  return eshPager;
};

Then('I see paginator', async () => {
  const pager = await getPaginator();
  chai.expect(pager).to.not.be.null;
});

Then("I don't see paginator", async () => {
  const pager = await getPaginator();
  chai.expect(pager).to.be.null;
});

Then("I don't see {string} button", async type => {
  const opacity = await getButtonOpacity(type);
  chai.expect(opacity).to.be.equal('0');
});

Then('I see {string} button', async type => {
  const opacity = await getButtonOpacity(type);
  chai.expect(opacity).to.be.equal('1');
});

Then('I see {int} of {int} items', async (expectedItems, expectedTotalItems) => {
  const pager = await getPagerValues();

  chai.expect(pager.items).to.be.equal(expectedItems);
  chai.expect(pager.totalItems).to.be.equal(expectedTotalItems);
});

Then('I see {int} of {int} pages', async (expectedPage, expectedTotalPages) => {
  const pager = await getPagerValues();

  chai.expect(pager.page).to.be.equal(expectedPage);
  chai.expect(pager.totalPages).to.be.equal(expectedTotalPages);
});

Then('I see message {string}', async message => {
  const text = await scope.page.evaluate(
    () =>
      document
        .querySelector('esh-app')
        .shadowRoot.querySelector('esh-catalog')
        .shadowRoot.querySelector('.no-results').textContent,
  );
  chai.expect(text).to.equalIgnoreSpaces(message);
});

Then('I see paginator text {string}', async expectedText => {
  const text = await getPagerText();
  chai.expect(text).to.equalIgnoreSpaces(expectedText);
});

When('I click {string} button', async type => {
  await clickButtion(type);
});
