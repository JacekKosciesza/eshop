const { When, Then } = require('cucumber');
const chai = require('chai');
chai.use(require('chai-string'));
const scope = require('../support/scope');

When('I go to {string} page', async path => {
  const { page } = scope;

  await page.goto(`http://localhost:8080/${path}`, {
    waitUntil: 'networkidle2',
  });
  // await page.waitFor(1000);
});

Then('I see {string} page', path => {
  const { page } = scope;

  chai.expect(page.url()).endsWith(path);
});

When("I click browser's {string} button", async type => {
  const { page } = scope;

  if (type === 'back') {
    await page.goBack();
  } else if (type === 'forward') {
    await page.goForward();
  }
});

When('I wait {int} milliseconds', async ms => {
  await scope.page.waitFor(ms);
});
