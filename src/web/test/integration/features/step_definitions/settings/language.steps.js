const { When, Then } = require('cucumber');
const chai = require('chai');
chai.use(require('chai-string'));
const scope = require('../../support/scope');

When('I change languge to {string}', async lang => {
  const { page } = scope;

  await page.evaluate(optionValue => {
    const eshSettings = document.querySelector('esh-app').shadowRoot.querySelector('esh-settings')
      .shadowRoot;

    /**
     * @type HTMLOptionElement
     */
    const option = eshSettings.querySelector(`option[value="${optionValue}"]`);
    option.selected = true;

    const select = eshSettings.querySelectorAll('select')[1];
    const event = new Event('change', { bubbles: true });
    select.dispatchEvent(event);
  }, lang);
});

Then('I see header with {string} link', async expectedText => {
  const { page } = scope;

  const text = await page.evaluate(() => {
    const eshHeader = document.querySelector('esh-app').shadowRoot.querySelector('esh-header')
      .shadowRoot;

    const a = eshHeader.querySelector('a:last-of-type');

    return a.textContent;
  });

  chai.expect(text).to.be.equal(expectedText);
});
