const { When, Then } = require('cucumber');
const chai = require('chai');
chai.use(require('chai-string'));
const scope = require('../../support/scope');

When('I change theme to {string}', async theme => {
  const { page } = scope;

  await page.evaluate(optionValue => {
    const eshSettings = document.querySelector('esh-app').shadowRoot.querySelector('esh-settings')
      .shadowRoot;

    /**
     * @type HTMLOptionElement
     */
    const option = eshSettings.querySelector(`option[value="${optionValue}"]`);
    option.selected = true;

    const select = eshSettings.querySelectorAll('select')[0];
    const event = new Event('change', { bubbles: true });
    select.dispatchEvent(event);
  }, theme);
});

Then('I see header which has {string} background color', async expectedColor => {
  const { page } = scope;

  const color = await page.evaluate(() => {
    const header = document.querySelector('esh-app').shadowRoot.querySelector('esh-header');
    return getComputedStyle(header).getPropertyValue('background-color');
  });

  chai.expect(color).to.be.equal(expectedColor);
});
