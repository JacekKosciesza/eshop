const { BeforeAll, Before, After, AfterAll } = require('cucumber');
const puppeteer = require('puppeteer');
const scope = require('./scope');

BeforeAll(async () => {
  // console.log('BeforeAll');
  // @ts-ignore
  scope.puppeteer = puppeteer;
  scope.browser = await scope.puppeteer.launch({
    headless: true,
    ignoreHTTPSErrors: true,
  });
});

Before(async () => {
  // console.log('Before');
  scope.context = await scope.browser.createIncognitoBrowserContext();
  scope.page = await scope.context.newPage();
});

After(async () => {
  // console.log('After');
  await scope.context.close();
});

AfterAll(async () => {
  // console.log('AfterAll');
  await scope.browser.close();
});
