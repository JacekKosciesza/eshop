import { html, fixture, expect, oneEvent } from '@open-wc/testing';

import '../../../src/catalog/esh-catalog-filters';
import { BRANDS, TYPES } from '../../../src/catalog/data';

describe('<esh-catalog-filters>', () => {
  it('created', async () => {
    const el = await fixture('esh-catalog-filters');
    expect(el).to.not.be.null;
  });

  it('set brands', async () => {
    const el = await fixture(
      html`
        <esh-catalog-filters .brands=${BRANDS}></esh-catalog-filters>
      `,
    );
    expect(el.brands).to.be.equal(BRANDS);
  });

  it('render brands', async () => {
    const el = await fixture(
      html`
        <esh-catalog-filters .brands=${BRANDS}></esh-catalog-filters>
      `,
    );
    const options = el.shadowRoot.querySelectorAll('.esh-catalog-filter.Brand option');
    expect(options.length).to.be.equal(BRANDS.length + 1);
  });

  it('set types', async () => {
    const el = await fixture(
      html`
        <esh-catalog-filters .types=${TYPES}></esh-catalog-filters>
      `,
    );
    expect(el.types).to.be.equal(TYPES);
  });

  it('render types', async () => {
    const el = await fixture(
      html`
        <esh-catalog-filters .types=${TYPES}></esh-catalog-filters>
      `,
    );
    const options = el.shadowRoot.querySelectorAll('.esh-catalog-filter.Type option');
    expect(options.length).to.be.equal(TYPES.length + 1);
  });

  describe('onFilterApplied', () => {
    it('should dispatch "filterApplied" event', async () => {
      const el = await fixture(
        html`
          <esh-catalog-filters></esh-catalog-filters>
        `,
      );
      setTimeout(() => el.onFilterApplied());
      await oneEvent(el, 'filterApplied');
    });
  });

  describe('onBrandFilterChanged', () => {
    it('should change "brandId"', async () => {
      const el = await fixture(
        html`
          <esh-catalog-filters></esh-catalog-filters>
        `,
      );
      el.onBrandFilterChanged({ target: { value: BRANDS[0].id } });
      expect(el.brandId).to.be.equal(BRANDS[0].id);
    });
  });

  describe('onTypeFilterChanged', () => {
    it('should change "typeId"', async () => {
      const el = await fixture(
        html`
          <esh-catalog-filters></esh-catalog-filters>
        `,
      );
      el.onTypeFilterChanged({ target: { value: TYPES[0].id } });
      expect(el.typeId).to.be.equal(TYPES[0].id);
    });
  });
});
