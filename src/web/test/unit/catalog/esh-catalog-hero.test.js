import { html, fixture, expect } from '@open-wc/testing';

import '../../../src/catalog/esh-catalog-hero';

describe('<esh-catalog-hero>', () => {
  it('created', async () => {
    const el = await fixture(
      html`
        <esh-catalog-hero></esh-catalog-hero>
      `,
    );
    expect(el).to.not.be.null;
  });
});
