import { html, fixture, expect } from '@open-wc/testing';

import { ITEMS } from '../../../src/catalog/data';
import '../../../src/catalog/esh-catalog-item';

describe('<esh-catalog-item>', () => {
  const items = ITEMS.map(item => ({ ...item, pictureUri: `../../..${item.pictureUri}` }));

  it('created', async () => {
    const el = await fixture(
      html`
        <esh-catalog-item></esh-catalog-item>
      `,
    );
    expect(el).to.not.be.null;
  });

  it('render item', async () => {
    const el = await fixture(
      html`
        <esh-catalog-item .item=${items[0]}></esh-catalog-item>
      `,
    );
    const img = el.shadowRoot.querySelector('.esh-catalog-thumbnail img');
    const src = img.getAttribute('src');
    expect(src).to.be.equal(items[0].pictureUri);
  });

  it('render item`s name uppercase', async () => {
    const el = await fixture(
      html`
        <esh-catalog-item .item=${items[0]}></esh-catalog-item>
      `,
    );
    const name = el.shadowRoot.querySelector('.esh-catalog-name').innerText;
    expect(name).to.be.equal(items[0].name.toUpperCase());
  });

  describe('addToCart', () => {
    it('should do nothing', async () => {
      const el = await fixture(
        html`
          <esh-catalog-item .item=${items[0]}></esh-catalog-item>
        `,
      );
      el.addToCart();
    });
  });
});
