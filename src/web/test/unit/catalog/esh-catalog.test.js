import { html, fixture, expect, oneEvent } from '@open-wc/testing';

import '../../../src/catalog/esh-catalog';
import { ITEMS, BRANDS, TYPES } from '../../../src/catalog/data';

describe('<esh-catalog>', () => {
  it('created', async () => {
    const el = await fixture(
      html`
        <esh-catalog></esh-catalog>
      `,
    );
    expect(el).to.not.be.null;
  });

  it('render items', async () => {
    const el = await fixture(
      html`
        <esh-catalog .items=${ITEMS}></esh-catalog>
      `,
    );
    const items = el.shadowRoot.querySelectorAll('esh-catalog-item');
    expect(items.length).to.be.equal(ITEMS.length);
  });

  describe('navigate', () => {
    it('should  dispatch "navigation" event', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const queryString = '?page=1';
      setTimeout(() => el.navigate(queryString));
      const { detail } = await oneEvent(el, 'navigation');

      expect(detail.location.search).to.be.equal(queryString);
    });

    // TODO
  });

  describe('addToCart', () => {
    it('should do nothing', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      el.addToCart();
    });
  });

  describe('buildQueryString', () => {
    it('return empty query string', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const queryString = el.buildQueryString();
      expect(queryString).to.be.equal('');
    });

    it('return query string with brandId', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const brandId = BRANDS[0].id;
      const typeId = null;
      const page = null;
      const queryString = el.buildQueryString(brandId, typeId, page);
      expect(queryString).to.be.equal(`?brandId=${brandId}`);
    });

    it('return query string with typeId', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const brandId = null;
      const typeId = TYPES[0].id;
      const page = null;
      const queryString = el.buildQueryString(brandId, typeId, page);
      expect(queryString).to.be.equal(`?typeId=${typeId}`);
    });

    it('return query string with page', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const brandId = null;
      const typeId = null;
      const page = 2;
      const queryString = el.buildQueryString(brandId, typeId, page);
      expect(queryString).to.be.equal(`?page=${page}`);
    });

    it('return query string with brandId, typeId and page', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const brandId = BRANDS[1].id;
      const typeId = TYPES[1].id;
      const page = 3;
      const queryString = el.buildQueryString(brandId, typeId, page);
      expect(queryString).to.be.equal(`?brandId=${brandId}&typeId=${typeId}&page=${page}`);
    });
  });

  describe('onPageChanged', () => {
    it('should update actualPage', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const e = new CustomEvent('changed', {
        detail: {
          page: 1,
        },
      });
      el.onPageChanged(e);
      expect(el.paginationInfo.actualPage).to.be.equal(1);
    });

    it('should dispatch "navigation" event', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const e = new CustomEvent('changed', {
        detail: {
          page: 2,
        },
      });
      setTimeout(() => el.onPageChanged(e));
      const { detail } = await oneEvent(el, 'navigation');
      expect(detail.location.search).to.be.equal(`?page=${e.detail.page}`);
    });
  });

  describe('onFilterApplied', () => {
    it('should set brandId', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const brandId = BRANDS[0].id;
      const e = { detail: { brandId } };

      el.onFilterApplied(e);
      expect(el.brandId).to.be.equal(brandId);
    });

    it('should set typeId', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const typeId = TYPES[0].id;
      const e = { detail: { typeId } };

      el.onFilterApplied(e);
      expect(el.typeId).to.be.equal(typeId);
    });

    it('should reset page', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const brandId = BRANDS[0].id;
      const typeId = TYPES[0].id;
      const e = { detail: { brandId, typeId } };

      el.onFilterApplied(e);
      expect(el.paginationInfo.actualPage).to.be.equal(0);
    });

    it('should dispatch "navigation" event', async () => {
      const brandId = BRANDS[1].id;
      const el = await fixture(
        html`
          <esh-catalog .brandId=${brandId}></esh-catalog>
        `,
      );
      const e = { detail: { brandId } };

      setTimeout(() => el.onFilterApplied(e));
      const { detail } = await oneEvent(el, 'navigation');
      expect(detail.location.search).to.be.equal(`?brandId=${brandId}`);
    });
  });

  describe('getBrands', () => {
    it('should update brands after fetch', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );

      const { fetch } = window;
      // @ts-ignore
      window.fetch = () => new Promise(resolve => resolve({ json: () => BRANDS }));
      await el.getBrands();
      expect(el.brands).to.be.eql(BRANDS);
      window.fetch = fetch;
    });

    it('should update brands after BrandsMock.fetch', async () => {
      const el = await fixture(
        html`
          <esh-catalog mockData></esh-catalog>
        `,
      );

      await el.getBrands();
      expect(el.brands).to.be.eql(BRANDS);
    });
  });

  describe('getTypes', () => {
    it('should update types after fetch', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );

      const { fetch } = window;
      // @ts-ignore
      window.fetch = () => new Promise(resolve => resolve({ json: () => TYPES }));
      await el.getTypes();
      expect(el.types).to.be.eql(TYPES);
      window.fetch = fetch;
    });

    it('should update types after TypesMock.fetch', async () => {
      const el = await fixture(
        html`
          <esh-catalog mockData></esh-catalog>
        `,
      );

      await el.getTypes();
      expect(el.types).to.be.eql(TYPES);
    });
  });

  describe('getItems', () => {
    it('should update items after fetch', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );

      const { fetch } = window;
      // @ts-ignore
      window.fetch = () => new Promise(resolve => resolve({ json: () => ({ items: ITEMS }) }));
      await el.getItems();
      expect(el.items).to.be.eql(ITEMS);
      window.fetch = fetch;
    });

    it('should update pagination info after fetch', async () => {
      const el = await fixture(
        html`
          <esh-catalog></esh-catalog>
        `,
      );
      const paginationInfo = {
        itemsPage: 10,
        totalItems: 12,
        actualPage: 1,
        totalPages: 2,
        items: 10,
      };

      const { fetch } = window;
      // @ts-ignore
      window.fetch = () => new Promise(resolve => resolve({ json: () => ({ paginationInfo }) }));
      await el.getItems();
      expect(el.paginationInfo).to.be.eql(paginationInfo);
      window.fetch = fetch;
    });

    it('should update types after ItemsMock.fetch', async () => {
      const el = await fixture(
        html`
          <esh-catalog mockData></esh-catalog>
        `,
      );

      await el.getItems();
      expect(el.items).to.be.eql(ITEMS.slice(0, 10));
    });

    it('should update pagination info  after ItemsMock.fetch', async () => {
      const el = await fixture(
        html`
          <esh-catalog mockData></esh-catalog>
        `,
      );
      const paginationInfo = {
        itemsPage: 10,
        totalItems: 12,
        actualPage: 1,
        totalPages: 2,
        items: 10,
      };

      await el.getItems();
      expect(el.paginationInfo).to.be.eql(paginationInfo);
    });
  });

  describe('attributeChangedCallback', () => {
    it('should set items', async () => {
      const el = await fixture(
        html`
          <esh-catalog mockData></esh-catalog>
        `,
      );
      const queryString = '?page=2';

      await el.attributeChangedCallback('querystring', '', queryString);
      expect(el.items).to.be.eql(ITEMS.slice(10, 12));
    });
  });
});
