import { html, fixture, expect, oneEvent } from '@open-wc/testing';

import '../../../src/catalog/esh-pager';

describe('<esh-pager>', () => {
  it('created', async () => {
    const el = await fixture('esh-pager');
    expect(el).to.not.be.null;
  });

  it('sets model', async () => {
    const paginationInfo = {
      itemsPage: 10,
      totalItems: 12,
      actualPage: 0,
      totalPages: 2,
      items: 10,
    };
    const el = await fixture(
      html`
        <esh-pager .model=${paginationInfo}></esh-pager>
      `,
    );
    expect(el.model).to.equal(paginationInfo);
  });

  describe('onNextClicked', () => {
    it('emits changed event', async () => {
      const paginationInfo = {
        itemsPage: 10,
        totalItems: 12,
        actualPage: 0,
        totalPages: 2,
        items: 10,
      };
      const el = await fixture(
        html`
          <esh-pager .model=${paginationInfo}></esh-pager>
        `,
      );

      const e = new CustomEvent('');
      setTimeout(() => el.onNextClicked(e));

      const { detail } = await oneEvent(el, 'changed');
      expect(detail.page).to.be.equal(1);
    });
  });

  describe('onPreviousClicked', () => {
    it('emits changed event', async () => {
      const paginationInfo = {
        itemsPage: 10,
        totalItems: 12,
        actualPage: 1,
        totalPages: 2,
        items: 10,
      };
      const el = await fixture(
        html`
          <esh-pager .model=${paginationInfo}></esh-pager>
        `,
      );

      const e = new CustomEvent('');
      setTimeout(() => el.onPreviousClicked(e));

      const { detail } = await oneEvent(el, 'changed');
      expect(detail.page).to.be.equal(0);
    });
  });
});
