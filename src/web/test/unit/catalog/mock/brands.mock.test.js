import { expect } from '@open-wc/testing';

import { BrandsMock } from '../../../../src/catalog/mock/brands.mock';
import { BRANDS } from '../../../../src/catalog/data';

describe('BrandsMock', () => {
  describe('fetch', () => {
    it('returns BRANDS', async () => {
      const brands = await BrandsMock.fetch();
      expect(brands).to.equal(BRANDS);
    });
  });
});
