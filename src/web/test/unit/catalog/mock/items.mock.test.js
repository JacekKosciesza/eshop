import { expect } from '@open-wc/testing';

import { ItemsMock } from '../../../../src/catalog/mock/items.mock';
import { ITEMS } from '../../../../src/catalog/data';

describe('ItemsMock', () => {
  describe('fetch', () => {
    it('returns 1st page of ITEMS', async () => {
      const response = await ItemsMock.fetch();
      expect(response).to.eql({
        items: ITEMS.slice(0, 10),
        paginationInfo: {
          itemsPage: 10,
          totalItems: 12,
          actualPage: 1,
          totalPages: 2,
          items: 10,
        },
      });
    });

    it('returns 2nd page of ITEMS', async () => {
      const queryString = '?page=2';
      const response = await ItemsMock.fetch(queryString);
      expect(response).to.eql({
        items: ITEMS.slice(10, 12),
        paginationInfo: {
          itemsPage: 10,
          totalItems: 12,
          actualPage: 2,
          totalPages: 2,
          items: 2,
        },
      });
    });

    it('returns `.NET Black & White Mug`', async () => {
      const queryString = '?brandId=1&typeId=1';
      const response = await ItemsMock.fetch(queryString);
      expect(response.items.length).to.equal(1);
      expect(response.items[0].name).to.equal('.NET Black & White Mug');
    });
  });
});
