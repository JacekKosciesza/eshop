import { expect } from '@open-wc/testing';

import { TypesMock } from '../../../../src/catalog/mock/types.mock';
import { TYPES } from '../../../../src/catalog/data';

describe('TypesMock', () => {
  describe('fetch', () => {
    it('returns TYPES', async () => {
      const types = await TypesMock.fetch();
      expect(types).to.equal(TYPES);
    });
  });
});
