import { html, fixture, expect } from '@open-wc/testing';

import '../../src/esh-app';

describe('<esh-app>', () => {
  it('created', async () => {
    await fixture(
      html`
        <esh-app mockData></esh-app>
      `,
    );
  });

  describe('onNavigation', () => {
    it('change page to settings', async () => {
      const el = await fixture(
        html`
          <esh-app mockData></esh-app>
        `,
      );
      const e = { detail: { location: { pathname: '/settings' } } };

      el.onNavigation(e);
      expect(el._page).to.equal('settings');
    });
  });

  describe('handleNavigation', () => {
    it('catalog is the default page', async () => {
      const el = await fixture(
        html`
          <esh-app mockData></esh-app>
        `,
      );
      el.handleNavigation({ pathname: '/' });
      expect(el._page).to.equal('catalog');
    });

    it('sets correctly settings page', async () => {
      const el = await fixture(
        html`
          <esh-app mockData></esh-app>
        `,
      );
      el.handleNavigation({ pathname: '/settings' });
      expect(el._page).to.equal('settings');
    });
  });

  describe('_loadPage', () => {
    it('does nothing for unknown page', async () => {
      const el = await fixture(
        html`
          <esh-app mockData></esh-app>
        `,
      );
      const page = 'unknown-page';

      el._loadPage(page);
    });
  });

  describe('onThemeChanged', () => {
    it('default theme clears class list', async () => {
      const el = await fixture(
        html`
          <esh-app mockData></esh-app>
        `,
      );
      const theme = 'default';
      const e = { detail: { theme } };

      el.onThemeChanged(e);
      expect([...el.classList]).to.is.empty;
    });

    it('theme is included in class list', async () => {
      const el = await fixture(
        html`
          <esh-app mockData></esh-app>
        `,
      );
      const theme = 'contrast';
      const e = { detail: { theme } };

      el.onThemeChanged(e);
      expect([...el.classList]).to.include(theme);
    });
  });

  describe('updateOnlineStatus', () => {
    it('removes `offline` class', async () => {
      const el = await fixture(
        html`
          <esh-app mockData></esh-app>
        `,
      );
      el.classList.add('offline');

      el.updateOnlineStatus();
      expect([...el.classList]).is.empty;
    });
  });

  describe('updateOfflineStatus', () => {
    it('adds `offline` class', async () => {
      const el = await fixture(
        html`
          <esh-app mockData></esh-app>
        `,
      );

      el.updateOfflineStatus();
      expect([...el.classList]).to.include('offline');
    });
  });
});
