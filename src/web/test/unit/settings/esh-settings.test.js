import { html, fixture, expect, oneEvent } from '@open-wc/testing';

import '../../../src/settings/esh-settings';

describe('<esh-settings>', () => {
  it('created', async () => {
    await fixture(
      html`
        <esh-settings></esh-settings>
      `,
    );
  });

  it('contains `Settings` header', async () => {
    const el = await fixture(
      html`
        <esh-settings></esh-settings>
      `,
    );
    const h1 = el.shadowRoot.querySelector('h1').innerText;

    expect(h1.toLowerCase()).to.have.string('settings');
  });

  describe('onThemeChanged', () => {
    it('emits themeChanged event', async () => {
      const el = await fixture(
        html`
          <esh-settings></esh-settings>
        `,
      );

      const theme = 'default';
      const e = { target: { value: theme } };
      setTimeout(() => el.onThemeChanged(e));

      const { detail } = await oneEvent(el, 'themeChanged');
      expect(detail.theme).to.be.equal(theme);
    });
  });

  describe('onLanguageChanged', () => {
    it('changes lang', async () => {
      const el = await fixture(
        html`
          <esh-settings></esh-settings>
        `,
      );

      const lang = 'pl';
      const e = { target: { value: lang } };
      el.onLanguageChanged(e);

      expect(el.lang).to.be.equal(lang);
    });
  });
});
