import { html, fixture, expect } from '@open-wc/testing';

import '../../../src/shared/esh-footer';

describe('<esh-footer>', () => {
  it('created', async () => {
    const el = await fixture(
      html`
        <esh-footer></esh-footer>
      `,
    );
    expect(el).to.not.be.null;
  });

  it('render logo without envrionment info', async () => {
    const env = '';
    const el = await fixture(
      html`
        <esh-footer .env=${env}></esh-footer>
      `,
    );
    const logoType = el.shadowRoot.querySelector('.logo').innerText;

    expect(logoType).to.equal('eSHOP\nonCONTAINERS');
  });

  it('render logo with envrionment info', async () => {
    const env = 'Test';
    const el = await fixture(
      html`
        <esh-footer .env=${env}></esh-footer>
      `,
    );
    const logoType = el.shadowRoot.querySelector('.logotype').innerText;

    expect(logoType).to.equal('eSHOP [Test]\nonCONTAINERS');
  });

  it('render copyright', async () => {
    const el = await fixture(
      html`
        <esh-footer></esh-footer>
      `,
    );
    const logoType = el.shadowRoot.querySelector('.copyright').innerText;

    expect(logoType).to.equal('e-ShopOnContainers. By Code & Pepper');
  });
});
