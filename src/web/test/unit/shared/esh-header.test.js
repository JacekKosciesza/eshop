import { html, fixture, expect } from '@open-wc/testing';

import '../../../src/shared/esh-header';

describe('<esh-header>', () => {
  it('created', async () => {
    const el = await fixture(
      html`
        <esh-header></esh-header>
      `,
    );
    expect(el).to.not.be.null;
  });

  it('render logo without envrionment info', async () => {
    const env = '';
    const el = await fixture(
      html`
        <esh-header .env=${env}></esh-header>
      `,
    );
    const logoType = el.shadowRoot.querySelector('.logotype').innerText;

    expect(logoType).to.equal('eSHOP\nonCONTAINERS');
  });

  it('render logo with envrionment info', async () => {
    const env = 'Test';
    const el = await fixture(
      html`
        <esh-header .env=${env}></esh-header>
      `,
    );
    const logoType = el.shadowRoot.querySelector('.logotype').innerText;

    expect(logoType).to.equal('eSHOP [Test]\nonCONTAINERS');
  });

  it('render home link', async () => {
    const el = await fixture(
      html`
        <esh-header></esh-header>
      `,
    );
    const a = el.shadowRoot.querySelector('a:first-of-type');
    const src = a.getAttribute('href');

    expect(src).to.equal('/');
  });

  it('render settings link', async () => {
    const el = await fixture(
      html`
        <esh-header></esh-header>
      `,
    );
    const a = el.shadowRoot.querySelector('a:last-of-type');
    const src = a.getAttribute('href');
    const text = a.innerText;

    expect(src).to.equal('/settings');
    expect(text).to.have.string('SETTINGS');
  });
});
