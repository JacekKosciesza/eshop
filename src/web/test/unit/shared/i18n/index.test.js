import { expect } from '@open-wc/testing';

import lang from '../../../../src/shared/i18n';

describe('i18n', () => {
  describe('lang', () => {
    it('returns `en` translations by default', async () => {
      const translation = await lang('en');

      expect(translation.settings).to.equal('Settings');
    });

    it('returns `pl` translations', async () => {
      const translation = await lang('pl');

      expect(translation.settings).to.equal('Ustawienia');
    });
  });
});
